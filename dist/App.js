"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("react-native-gesture-handler");
const react_1 = __importDefault(require("react"));
const react_native_1 = require("react-native");
const CompanyProvider_1 = require("./source/providers/CompanyProvider");
const providers_1 = require("./source/providers");
const Navigator_1 = __importDefault(require("./source/Navigator"));
const Language_1 = __importDefault(require("./source/environments/languages/Language"));
const Strings_en_GB_1 = __importDefault(require("./source/environments/languages/Strings-en_GB"));
const Strings_it_IT_1 = __importDefault(require("./source/environments/languages/Strings-it_IT"));
const Strings_es_ES_1 = __importDefault(require("./source/environments/languages/Strings-es_ES"));
const Strings_fr_FR_1 = __importDefault(require("./source/environments/languages/Strings-fr_FR"));
const AppEnvironment_1 = require("./source/environments/AppEnvironment");
const user_1 = require("./source/network/user");
const userLanguage = async () => {
    const companyId = AppEnvironment_1.isCustomApp() && AppEnvironment_1.AppCompanyId;
    const data = await user_1.getUserData(companyId);
    if (data && data.language) {
        if (data.language === "en") {
            global.strings = Strings_en_GB_1.default;
        }
        else if (data.language === "it") {
            global.strings = Strings_it_IT_1.default;
        }
        else if (data.language === "es") {
            global.strings = Strings_es_ES_1.default;
        }
        else if (data.language === "fr") {
            global.strings = Strings_fr_FR_1.default;
        }
    }
};
global.videoEachStep = 9;
global.strings = Language_1.default();
userLanguage();
// without this, if the app starts in landscape orientation the streamview will bug
let param = {
    w: react_native_1.Dimensions.get("screen").width,
    h: react_native_1.Dimensions.get("screen").height,
};
global.screenProp = {
    width: param.w < param.h ? param.w : param.h,
    height: param.w < param.h ? param.h : param.w,
};
// end comment
let App = /** @class */ (() => {
    class App extends react_1.default.Component {
        constructor() {
            super(...arguments);
            this.componentDidMount = async () => {
                const companyId = AppEnvironment_1.isCustomApp() && AppEnvironment_1.AppCompanyId;
                const data = await user_1.getUserData(companyId);
                if (data && data.language) {
                    if (data.language === "en") {
                        global.strings = Strings_en_GB_1.default;
                    }
                    else if (data.language === "it") {
                        global.strings = Strings_it_IT_1.default;
                    }
                    else if (data.language === "es") {
                        global.strings = Strings_es_ES_1.default;
                    }
                    else if (data.language === "fr") {
                        global.strings = Strings_fr_FR_1.default;
                    }
                }
            };
        }
        render() {
            return (<providers_1.AppProvider>
        <Navigator_1.default />
      </providers_1.AppProvider>);
        }
    }
    App.contextType = CompanyProvider_1.ActiveCompanyContext;
    return App;
})();
exports.default = App;
// Live slider with live duration / offset from current time, conditional dot red / grey -- Monday
// GoogleCast -- Monday
// ComponentWillMount
// ScrollView + Flatlist
// update to use createAnimatedSwitchNavigator()
// Video element props
// smoothSkinLevel: 0 - 5
// denoise: bool -- mute?
// video: { preset: any number?, profile: 0 - 2 }
// Video presets
// public static final int VIDEO_PPRESET_16X9_270 = 0;
// public static final int VIDEO_PPRESET_16X9_360 = 1;
// public static final int VIDEO_PPRESET_16X9_480 = 2;
// public static final int VIDEO_PPRESET_16X9_540 = 3;
// public static final int VIDEO_PPRESET_16X9_720 = 4;
// Video profiles
// public static final int VIDEO_PROFILE_BASELINE = 0;
// public static final int VIDEO_PROFILE_MAIN = 1;
// public static final int VIDEO_PROFILE_HIGH = 2;