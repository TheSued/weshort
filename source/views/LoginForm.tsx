import React from "react";
import { View, Image, StyleSheet, Keyboard, Text } from "react-native";

import { ActiveCompanyContext } from "../providers";
import { LoginButton } from "../components";
import {
  Background,
  AppText,
  Input,
  Expand,
  Touchable,
} from "../components/common";

import Colors from "../environments/Colors";

import Storage from "../services/Storage";
import * as Network from "../network";
import { isCustomApp } from "../environments/AppEnvironment";

interface LoginProps {
  navigation: any;
}
import {
  OrientationLocker,
  PORTRAIT,
  LANDSCAPE,
} from "react-native-orientation-locker";

class LoginForm extends React.PureComponent<LoginProps> {
  static contextType = ActiveCompanyContext;

  state = {
    email: "",
    password: "",
    isLoading: true,
    isLogging: false,
    showError: false,
    message: "",
    anim: false,
    pageHasFocus: false,
  };

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", () =>
      this.setState({ pageHasFocus: true })
    );
    this.blurListener = this.props.navigation.addListener("blur", () =>
      this.setState({ pageHasFocus: false })
    );
  }

  navigateToRegistration() {
    this.props.navigation.navigate("Registration");
  }

  navigateToHomepage() {
    this.props.navigation.navigate("Homepage");
  }

  onError(message: string) {
    this.setState({ message: message }, () =>
      setTimeout(() => this.setState({ showError: true }), 500)
    );
  }

  async onSubmit() {
    Keyboard.dismiss();

    if (this.state.email !== "" && this.state.password !== "") {
      this.setState({ isLogging: true, message: "", showError: false });
      setTimeout(async () => {
        const token = await Network.login(
          this.state.email,
          this.state.password
        );
        if (token !== -1 && token !== -2) {
          await Storage.saveToken(token);
          this.context.signedOut();
        } else if (token === -1) {
          this.setState({ isLogging: false }, () =>
            this.onError(global.strings.CredentialsError)
          );
        } else {
          this.setState({ isLogging: false }, () =>
            this.onError(global.strings.NetworkError)
          );
        }
      }, 500);
    } else {
      this.onError(global.strings.NoCredentials);
    }
  }

  renderView() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Image
          resizeMode={"contain"}
          source={{ uri: `${this.context.activeCompany.logo}` }}
          style={styles.imageStyle}
        />

        <Expand
          params={{ start: 0, end: 30 }}
          controller={this.state.message == "" ? false : true}
          style={{ marginTop: 13 }}
        >
          <AppText style={styles.errorTextStyle}>
            {this.state.showError ? this.state.message : ""}
          </AppText>
        </Expand>

        <View
          style={[
            styles.formContainer1,
            { backgroundColor: this.context.alphaColor },
          ]}
        >
          <Input
            autoCapitalize={"none"}
            autoCorrect={false}
            onChangeText={(text: string) => this.setState({ email: text })}
            value={this.state.email}
            placeholder={global.strings.EmailPlaceholder}
            style={[
              styles.formInput,
              {
                color: this.context.fontColor,
              },
            ]}
          />
        </View>

        <View
          style={[
            styles.formContainer2,
            { backgroundColor: this.context.alphaColor },
          ]}
        >
          <Input
            onChangeText={(text: string) => this.setState({ password: text })}
            value={this.state.password}
            placeholder={global.strings.PasswordPlaceholder}
            secureTextEntry
            style={[
              styles.formInput,
              {
                color: this.context.fontColor,
              },
            ]}
          />
        </View>

        <LoginButton
          color={this.context.fontColor}
          style={{
            borderColor: this.context.buttonColor,
          }}
          controller={this.state.isLogging}
          onSubmit={() => this.onSubmit()}
        />

        {this.context.activeCompany.registration_allowed && (
          <View style={{ alignItems: "center" }}>
            <AppText style={{ marginTop: 30, color: this.context.fontColor }}>
              {global.strings.NoAccount}
            </AppText>
            <Touchable onPress={() => this.navigateToRegistration()}>
              <AppText
                style={{
                  marginTop: 12,
                  fontWeight: "bold",
                  fontSize: 16,
                  textDecorationLine: "underline",
                  color: this.context.fontColor,
                }}
              >
                {global.strings.SignUp}
              </AppText>
            </Touchable>
          </View>
        )}
        {this.context.activeCompany.home_guest && (
          <Touchable
            style={{
              alignItems: "center",
              marginTop: 16,
              color: this.context.fontColor,
            }}
            onPress={() => this.navigateToHomepage()}
          >
            <AppText
              style={{
                marginVertical: 12,
                fontSize: 16,
                color: this.context.fontColor,
              }}
            >
              {global.strings.Skip}
            </AppText>
          </Touchable>
        )}
      </View>
    );
  }

  render() {
    if (this.state.pageHasFocus) {
      return (
        <Background login style={styles.backgroundStyle}>
          <OrientationLocker orientation={PORTRAIT} />
          {this.renderView()}
        </Background>
      );
    } else {
      return <Background empty />;
    }
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },

  // logo
  backgroundStyle: {
    justifyContent: "space-around",
    alignItems: "center",
  },
  imageStyle: {
    height: 137,
    width: 220,
    alignSelf: "center",
    marginBottom: 24,
  },

  // error text
  errorTextStyle: {
    alignSelf: "center",
    color: Colors.TextError,
    fontSize: 17,
    elevation: 1,
  },

  buttonContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25,
    borderWidth: 1,
    borderStyle: "solid",
    elevation: 1,
  },
  buttonText: {
    fontSize: 22,
    paddingTop: 12,
    paddingBottom: 12,
    elevation: 1,
  },

  formContainer1: {
    width: 300,

    marginTop: 13,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  formContainer2: {
    width: 300,

    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    marginTop: 2,
  },
  formInput: {
    fontSize: 20,
    height: 60,
    paddingLeft: 15,
  },
  logoStyle: {
    height: 134,
    width: 200,
  },
  footerContainer: {
    textAlign: "center",
    marginTop: 100,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  footerText: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 15,
  },
  footerCompanyTitle: {
    marginTop: 10,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 11,
  },
});

export { LoginForm };
