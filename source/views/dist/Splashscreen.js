"use strict";
var __extends =
  (this && this.__extends) ||
  (function () {
    var extendStatics = function (d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function (d, b) {
            d.__proto__ = b;
          }) ||
        function (d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function (d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype =
        b === null
          ? Object.create(b)
          : ((__.prototype = b.prototype), new __());
    };
  })();
var __assign =
  (this && this.__assign) ||
  function () {
    __assign =
      Object.assign ||
      function (t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];
          for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
      };
    return __assign.apply(this, arguments);
  };
var __awaiter =
  (this && this.__awaiter) ||
  function (thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function (resolve) {
            resolve(value);
          });
    }
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
var __generator =
  (this && this.__generator) ||
  function (thisArg, body) {
    var _ = {
        label: 0,
        sent: function () {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: [],
      },
      f,
      y,
      t,
      g;
    return (
      (g = { next: verb(0), throw: verb(1), return: verb(2) }),
      typeof Symbol === "function" &&
        (g[Symbol.iterator] = function () {
          return this;
        }),
      g
    );
    function verb(n) {
      return function (v) {
        return step([n, v]);
      };
    }
    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");
      while (_)
        try {
          if (
            ((f = 1),
            y &&
              (t =
                op[0] & 2
                  ? y["return"]
                  : op[0]
                  ? y["throw"] || ((t = y["return"]) && t.call(y), 0)
                  : y.next) &&
              !(t = t.call(y, op[1])).done)
          )
            return t;
          if (((y = 0), t)) op = [op[0] & 2, t.value];
          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;
            case 4:
              _.label++;
              return { value: op[1], done: false };
            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;
            case 7:
              op = _.ops.pop();
              _.trys.pop();
              continue;
            default:
              if (
                !((t = _.trys), (t = t.length > 0 && t[t.length - 1])) &&
                (op[0] === 6 || op[0] === 2)
              ) {
                _ = 0;
                continue;
              }
              if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                _.label = op[1];
                break;
              }
              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }
              if (t && _.label < t[2]) {
                _.label = t[2];
                _.ops.push(op);
                break;
              }
              if (t[2]) _.ops.pop();
              _.trys.pop();
              continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      if (op[0] & 5) throw op[1];
      return { value: op[0] ? op[1] : void 0, done: true };
    }
  };
exports.__esModule = true;
exports.Splashscreen = void 0;
var react_1 = require("react");
var react_native_1 = require("react-native");
var Storage_1 = require("../services/Storage");
var Notifications_utils_1 = require("../utils/Notifications.utils");
var AppEnvironment_1 = require("../environments/AppEnvironment");
var Network = require("../network");
var providers_1 = require("../providers");
var AppStatusBar_1 = require("../components/common/AppStatusBar/AppStatusBar");
var common_1 = require("../components/common");
var Splashscreen = /** @class */ (function (_super) {
  __extends(Splashscreen, _super);
  function Splashscreen() {
    var _this = (_super !== null && _super.apply(this, arguments)) || this;
    _this.state = {
      verifyMailStep: false,
    };
    return _this;
  }
  Splashscreen.prototype.componentDidMount = function () {
    this.initializeApp();
  };
  Splashscreen.prototype.initializeApp = function () {
    return __awaiter(this, void 0, void 0, function () {
      var token, notification;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4 /*yield*/, this.setCompany()];
          case 1:
            _a.sent();
            return [4 /*yield*/, Storage_1["default"].getToken()];
          case 2:
            token = _a.sent();

            if (!token) return [3 /*break*/, 5];
            return [
              4 /*yield*/,
              Notifications_utils_1.getInitialNotifications(),
            ];
          case 3:
            notification = _a.sent();
            return [4 /*yield*/, this.initializeUser(notification)];
          case 4:
            _a.sent();
            return [2 /*return*/];
          case 5:
            this.context.signedIn(null);
            _a.label = 6;
          case 6:
            return [2 /*return*/];
        }
      });
    });
  };
  Splashscreen.prototype.initializeUser = function (notification) {
    return __awaiter(this, void 0, void 0, function () {
      var lanSuffix, locale, data, userData, companySettings, companyData;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            lanSuffix = "";
            locale = this.getLocale();
            if (locale.includes("it_")) {
              lanSuffix = "it";
              this.context.setCompanySettingsLanguage(lanSuffix);
            } else if (locale.includes("en_")) {
              lanSuffix = "en";
              this.context.setCompanySettingsLanguage(lanSuffix);
            } else if (locale.includes("es_")) {
              lanSuffix = "es";
              this.context.setCompanySettingsLanguage(lanSuffix);
            } else if (locale.includes("fr_")) {
              lanSuffix = "fr";
              this.context.setCompanySettingsLanguage(lanSuffix);
            }
            return [4 /*yield*/, this.getData()];
          case 1:
            data = _a.sent();
            if (!data) return [3 /*break*/, 3];
            userData = this.formatUser(data);
            return [
              4 /*yield*/,
              Network.getCompanySettings(
                data.companies[0].id,
                react_native_1.Platform.OS,
                this.context.languageCompanySettings
              ),
            ];
          case 2:
            companySettings = _a.sent();
            companyData = this.formatCompany(data, companySettings);
            if (!data.email_verified) {
              this.setState({ verifyMailStep: true });
              return [2 /*return*/];
            }
            if (notification) {
              this.context.setVideoNotificationId(notification);
            }
            this.context.signedIn(userData);
            return [2 /*return*/];
          case 3:
            this.context.signedIn(null);
            _a.label = 4;
          case 4:
            return [2 /*return*/];
        }
      });
    });
  };
  Splashscreen.prototype.getLocale = function () {
    switch (react_native_1.Platform.OS) {
      case "ios":
        return (
          (react_native_1.NativeModules.SettingsManager.settings.AppleLocale &&
            react_native_1.NativeModules.SettingsManager.settings.AppleLocale.replace(
              "-",
              "_"
            )) ||
          react_native_1.NativeModules.SettingsManager.settings
            .AppleLanguages[0]
        );
      case "android":
        return react_native_1.NativeModules.I18nManager.localeIdentifier;
    }
    return "";
  };
  Splashscreen.prototype.setCompany = function () {
    return __awaiter(this, void 0, void 0, function () {
      var lanSuffix, locale, companyData, companySettings;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            lanSuffix = "";
            locale = this.getLocale();
            if (locale.includes("it_")) {
              lanSuffix = "it";
              this.context.setCompanySettingsLanguage(lanSuffix);
            } else if (locale.includes("en_")) {
              lanSuffix = "en";
              this.context.setCompanySettingsLanguage(lanSuffix);
            } else if (locale.includes("es_")) {
              lanSuffix = "es";
              this.context.setCompanySettingsLanguage(lanSuffix);
            } else if (locale.includes("fr_")) {
              lanSuffix = "fr";
              this.context.setCompanySettingsLanguage(lanSuffix);
            }
            return [
              4 /*yield*/,
              Network.getCompanySettings(
                AppEnvironment_1.AppCompanyId,
                react_native_1.Platform.OS,
                this.context.languageCompanySettings
              ),
            ];
          case 1:
            companyData = _a.sent();
            companySettings = JSON.parse(JSON.stringify(companyData));

            this.context.changeActiveCompany(
              __assign(
                { id: AppEnvironment_1.AppCompanyId },
                JSON.parse(JSON.stringify(companyData))
              )
            );

            this.context.handleCompanyColors(
              companySettings.skin.schema,
              companySettings.skin.primaryColor,
              companySettings.skin.backgroundColor,
              companySettings.skin.font
            );
            return [2 /*return*/];
        }
      });
    });
  };
  Splashscreen.prototype.getData = function () {
    return __awaiter(this, void 0, void 0, function () {
      var companyId, data;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            companyId =
              AppEnvironment_1.isCustomApp() && AppEnvironment_1.AppCompanyId;
            return [4 /*yield*/, Network.getUserData(companyId)];
          case 1:
            data = _a.sent();
            return [2 /*return*/, data];
        }
      });
    });
  };
  Splashscreen.prototype.formatUser = function (data) {
    return {
      displayName: data.display_name,
      profileImage: data.profile_image,
      role: data.role,
      work: data.work,
      followers: data.followers,
      live: data.live,
      email: data.email,
      sid: data.sid,
      creator: data.companies[0].creator,
    };
  };
  Splashscreen.prototype.formatCompany = function (
    data,
    activeCompanySettings
  ) {
    var _a = this.context.activeCompany,
      logo = _a.logo,
      default_video_image = _a.default_video_image;
    data.companies.forEach(function (c) {
      c.logo = c.logo || logo;
      c.default_video_image = c.default_video_image || default_video_image;
      c.image = c.image || default_video_image;
    });
    var activeCompany = data.companies.find(function (c) {
      return c.id === data.id_company;
    });
    return {
      companies: data.companies,
      activeCompany: __assign(
        __assign(__assign({}, activeCompany), activeCompanySettings),
        { id: data.id_company }
      ),
    };
  };
  Splashscreen.prototype.render = function () {
    var _this = this;
    return react_1["default"].createElement(
      react_1["default"].Fragment,
      null,
      react_1["default"].createElement(AppStatusBar_1["default"], null),
      react_1["default"].createElement(
        react_native_1.View,
        {
          style: {
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "black",
            flexDirection: "column",
          },
        },
        !this.state.verifyMailStep &&
          react_1["default"].createElement(
            common_1.Touchable,
            {
              onPress: function () {},
            },
            react_1["default"].createElement(react_native_1.Image, {
              resizeMode: "contain",
              source: require("../../assets/images/logo_mini.png"),
              style: styles.logoStyle,
            })
          ),
        this.state.verifyMailStep &&
          react_1["default"].createElement(
            react_1["default"].Fragment,
            null,
            react_1["default"].createElement(
              common_1.Touchable,
              {
                onPress: function () {},
              },
              react_1["default"].createElement(react_native_1.Image, {
                resizeMode: "contain",
                source: require("../../assets/images/logo_mini.png"),
                style: styles.logoEmailStyle,
              })
            ),
            react_1["default"].createElement(
              common_1.AppText,
              { style: styles.emailTitle },
              global.strings.VerifyEmailError
            ),
            react_1["default"].createElement(
              common_1.AppText,
              { style: styles.emailSubTitle },
              global.strings.VerifyEmail
            ),
            react_1["default"].createElement(
              common_1.Touchable,
              {
                style: {
                  padding: 15,
                  backgroundColor: "rgba(255,255,255,2)",
                  marginTop: 20,
                },
                onPress: function () {
                  return _this.setState({ verifyMailStep: false }, function () {
                    return _this.initializeUser(null);
                  });
                },
              },
              react_1["default"].createElement(
                common_1.AppText,
                { style: styles.buttonText },
                global.strings.TryAgain
              )
            ),
            react_1["default"].createElement(
              common_1.Touchable,
              {
                onPress: function () {
                  return __awaiter(_this, void 0, void 0, function () {
                    var _this = this;
                    return __generator(this, function (_a) {
                      switch (_a.label) {
                        case 0:
                          return [
                            4 /*yield*/,
                            Storage_1["default"].deleteToken(),
                          ];
                        case 1:
                          _a.sent();
                          this.context.signedOut();
                          this.setState({ verifyMailStep: false }, function () {
                            return _this.initializeApp();
                          });
                          return [2 /*return*/];
                      }
                    });
                  });
                },
                style: {
                  position: "absolute",
                  bottom: 0,
                  justifyContent: "center",
                  padding: 5,
                  marginBottom: 80,
                  borderBottomColor: "white",
                  borderBottomWidth: 2,
                },
              },
              react_1["default"].createElement(
                common_1.AppText,
                { style: styles.buttonTextLogout },
                "Logout"
              )
            )
          )
      )
    );
  };
  Splashscreen.contextType = providers_1.ActiveCompanyContext;
  return Splashscreen;
})(react_1["default"].PureComponent);
exports.Splashscreen = Splashscreen;
var styles = react_native_1.StyleSheet.create({
  logoStyle: {
    height: 134,
    width: 200,
  },
  logoEmailStyle: {
    height: 100,
    width: 100,
    marginBottom: 50,
  },
  emailTitle: {
    fontWeight: "700",
    fontSize: 25,
    marginBottom: 15,
  },
  emailSubTitle: {
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 40,
    marginRight: 40,
    marginBottom: 20,
    textAlign: "center",
  },
  buttonText: {
    fontWeight: "700",
    fontSize: 19,
    paddingHorizontal: 15,
  },
  buttonTextLogout: {
    fontWeight: "700",
    fontSize: 19,
    paddingHorizontal: 15,
    justifyContent: "flex-end",
    alignSelf: "flex-end",
  },
});
