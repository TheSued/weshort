"use strict";
exports.__esModule = true;
exports.castState = void 0;
var react_1 = require("react");
var react_native_1 = require("react-native");
var react_native_google_cast_1 = require("react-native-google-cast");
function Client(props) {
    var client = react_native_google_cast_1.useRemoteMediaClient();
    var status = react_native_google_cast_1.useMediaStatus();
    var streamPosition = react_native_google_cast_1.useStreamPosition();
    react_1.useEffect(function () {
        console.log(props.video);
        var started = client === null || client === void 0 ? void 0 : client.onMediaPlaybackStarted(function () {
            return console.log("playback started");
        });
        var ended = client === null || client === void 0 ? void 0 : client.onMediaPlaybackEnded(function () {
            return console.log("playback ended");
        });
        return function () {
            started === null || started === void 0 ? void 0 : started.remove();
            ended === null || ended === void 0 ? void 0 : ended.remove();
        };
    }, [client]);
    if (!client) {
        return (react_1["default"].createElement(react_native_1.Text, { style: { margin: 10 } }, "Connect to a Cast device to establish a session"));
    }
    return (react_1["default"].createElement(react_native_1.View, { style: { flex: 1 } },
        react_1["default"].createElement(react_native_1.ScrollView, { contentContainerStyle: { padding: 10 } },
            !status || status.playerState === react_native_google_cast_1.MediaPlayerState.IDLE ? (react_1["default"].createElement(react_native_1.Button, { onPress: function () {
                    return client.loadMedia({
                        mediaInfo: {
                            contentUrl: props.video,
                            contentType: "application/x-mpegURL"
                        }
                    });
                }, title: "Load Media" })) : status.playerState === react_native_google_cast_1.MediaPlayerState.PAUSED ? (react_1["default"].createElement(react_native_1.Button, { onPress: function () { return client.play(); }, title: "Play" })) : (react_1["default"].createElement(react_native_1.Button, { onPress: function () { return client.pause(); }, title: "Pause" })),
            streamPosition != null && (react_1["default"].createElement(react_native_1.View, { style: {
                    alignItems: "center",
                    flexDirection: "row",
                    justifyContent: "center",
                    marginTop: 10
                } },
                react_1["default"].createElement(react_native_1.Button, { onPress: function () { return client.seek({ position: -10, relative: true }); }, title: "-10s" }),
                react_1["default"].createElement(react_native_1.Text, { style: { margin: 5 } },
                    "Position: ",
                    streamPosition),
                react_1["default"].createElement(react_native_1.Button, { onPress: function () { return client.seek({ position: 10, relative: true }); }, title: "+10s" }))),
            status && (react_1["default"].createElement(react_native_1.View, { style: {
                    alignItems: "center",
                    flexDirection: "row",
                    justifyContent: "center",
                    marginTop: 10
                } },
                react_1["default"].createElement(react_native_1.Button, { disabled: status.playbackRate <= 0.5, onPress: function () {
                        return client.setPlaybackRate(round(status.playbackRate - 0.1));
                    }, title: "-0.1" }),
                react_1["default"].createElement(react_native_1.Text, { style: { margin: 5 } },
                    "Playback rate: ",
                    round(status.playbackRate)),
                react_1["default"].createElement(react_native_1.Button, { disabled: status.playbackRate >= 2.0, onPress: function () {
                        return client.setPlaybackRate(round(status.playbackRate + 0.1));
                    }, title: "+0.1" }))),
            status && (react_1["default"].createElement(react_native_1.View, { style: {
                    alignItems: "center",
                    flexDirection: "row",
                    justifyContent: "center",
                    marginTop: 10
                } },
                react_1["default"].createElement(react_native_1.Button, { disabled: status.volume <= 0.0, onPress: function () { return client.setStreamVolume(round(status.volume - 0.1)); }, title: "-0.1" }),
                react_1["default"].createElement(react_native_1.Text, { style: { margin: 5 } },
                    "Volume: ",
                    round(status.volume)),
                react_1["default"].createElement(react_native_1.Button, { disabled: status.volume >= 1.0, onPress: function () { return client.setStreamVolume(round(status.volume + 0.1)); }, title: "+0.1" }),
                react_1["default"].createElement(react_native_1.View, { style: { marginLeft: 10 } },
                    react_1["default"].createElement(react_native_1.Button, { onPress: function () { return client.setStreamMuted(!status.isMuted); }, title: status.isMuted ? "Unmute" : "Mute" })))),
            status && (react_1["default"].createElement(react_native_1.View, { style: { marginTop: 10 } },
                react_1["default"].createElement(react_native_1.Button, { onPress: function () { return client.stop(); }, title: "Stop" }))))));
}
function round(number, decimals) {
    if (decimals === void 0) { decimals = 1; }
    var factor = Math.pow(10, decimals);
    return Math.round(number * factor) / factor;
}
exports["default"] = Client;
exports.castState = function () {
    var _a = react_1["default"].useState(false), clientState = _a[0], setClientState = _a[1];
    var client = react_native_google_cast_1.useRemoteMediaClient();
    if (client) {
        setClientState(true);
    }
    else {
        setClientState(true);
    }
    return clientState;
};
