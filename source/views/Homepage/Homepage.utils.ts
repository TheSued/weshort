import * as Network from "../../network";
import { formatVideoObject } from "../../utils/common.utils";

export function getFunctionForItem(
  companyId: number,
  id: string,
  type: string,
  step: number
) {
  switch (type) {
    case "list_pack":
    case "header_slideshow_pack":
      return Network.getBundleList(
        companyId,
        step,
        id,
        type === "header_slideshow_pack"
      );
    case "list_serie":
    case "header_slideshow_serie":
      return Network.getSeries(
        companyId,
        step,
        id,
        type === "header_slideshow_serie"
      );
    case "list_video": {
      if (id === "live_now") {
        return Network.getVideo("live", step);
      } else if (id === "keep_watching") {
        return Network.getVideoKeepWatching(step);
      } else if (id === "live_scheduled") {
        return Network.getVideo("scheduled", step);
      } else {
        return Network.getSerieVideo(companyId, id, step);
      }
    }
    case "description_video":
      return Network.getVideoSource(+id);
    default:
      return null;
  }
}

export function needData(type: string): boolean {
  const valid = ["description_video"];
  return (
    valid.indexOf(type) !== -1 ||
    type.includes("list_") ||
    type.includes("header_slideshow_")
  );
}

export function mapData(type: string, data: any): any {
  if (type.includes("pack") || type.includes("serie")) {
    data.forEach((el) => (el.groupType = "serie"));
    return data;
  } else if (type === "description_video") {
    const videoInfo = formatVideoObject(data);
    const res = {
      source:
        videoInfo.fullhd || videoInfo.hd || videoInfo.hq || videoInfo.original,
      groupType: "video",
      ...data,
    };
    return res;
  } else {
    data.forEach((el) => (el.groupType = "video"));
    return data;
  }
}
