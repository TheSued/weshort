import React from "react";
import {
  View,
  ImageBackground,
  StyleSheet,
  Animated,
  FlatList,
  BackHandler,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  Platform,
  Button,
  Pressable,
} from "react-native";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faVolumeMute,
  faVolumeUp,
  faRedo,
  faLock,
  faLockOpen,
} from "@fortawesome/free-solid-svg-icons";
import {
  PanGestureHandler,
  TapGestureHandler,
} from "react-native-gesture-handler";
import LinearGradient from "react-native-linear-gradient";
import {
  OrientationLocker,
  PORTRAIT,
  LANDSCAPE,
} from "react-native-orientation-locker";

import Video from "react-native-video";

import { PulseIndicator } from "react-native-indicators";

import * as Network from "../network";
import Colors from "../environments/Colors";

import { Background, AppText, Touchable } from "../components/common";
import { Header, Modal, HomePageList } from "../components";
import { ActiveCompanyContext } from "../providers/CompanyProvider";
import { formatVideoObject } from "../utils/common.utils";

const currentScreen = "SerieView";

class SerieView extends React.Component {
  static contextType = ActiveCompanyContext;
  backHandler: any;
  overlayOpacity: Animated.Value;

  state = {
    access: false, // if the user can view this serie
    trailerVolumeMuted: true,
    // serie
    currentSerie: {
      id: null,
      data: [],
      step: 1,
      loading: false,
    },
    // trailer: false,
    errorMessage: "",
    packSeries: "",

    // switch between watch trailer and normal SerieView
    fullscreen: false,

    // trailer states
    screenHeight: (global.screenProp.width / 16) * 9,
    screenWidth: global.screenProp.width - 6,
    trailerSource: null,
    muted: true,
    currentTime: 0,
    secondsPlayed: 0,
    intervalId: 0,
    updateIntervalId: 0,

    bundleInfo: [],

    // these control the render of the pay buttons
    paySerie: false,
    paySubscription: false,
    subscription: {
      period: 0,
      price: 0,
    },
    subscriptionPrice: 0,
    subscriptionPeriod: 0,

    pageHasFocus: false,
    elementPreviewMarker: [],
    elementShowPreview: [],
  };

  // TODO: misleading fullscreen or trailer?
  end = 0;
  action = "";
  video: Video;
  trailerVolumeMuted = true;

  async getTrailer(trailer: any) {
    const videoInfo = await Network.getVideoSource(trailer);

    if (videoInfo) {
      let video = formatVideoObject(videoInfo);
      this.setState({
        trailerSource: video.fullhd || video.hd || video.hq || video.original,
      });
    }
  }

  async getBundleChild(serieId) {
    const { bundleInfo } = this.state;
    let step = 0;
    if (bundleInfo.length == 0) {
      step = 1; // first load
    } else if (bundleInfo.length % global.videoEachStep == 0) {
      step = bundleInfo.length / global.videoEachStep + 1; // if length == videoEachStep then load the next array
    }
    if (step != 0) {
      const data = await Network.getBundleChild(serieId, step);
      if (data && data.length) {
        this.setState({ bundleInfo: [...bundleInfo, ...data] });
      }
    }
  }

  async getSerieParent(serieId) {
    const data = await Network.getSerieParent(serieId);
    if (data && data.length) {
      this.setState({ bundleInfo: data });
    }
  }

  navigateToPayment(paymentData) {
    const { serie } = this.props.route.params;
    if (this.context.user) {
      this.props.navigation.navigate("Payment", {
        serie: serie,
        previousScreen: currentScreen,
        payment: paymentData.period,
        price: paymentData.price,
      });
    } else {
      this.props.navigation.navigate("Login", {
        previousScreen: currentScreen,
      });
    }
  }

  navigateToPaymentSerie(paymentData) {
    const { serie } = this.props.route.params;
    if (this.context.user) {
      this.props.navigation.navigate("PaymentSerie", {
        serie: serie.name,
        previousScreen: "currentScreen",
        payment: paymentData.period,
        price: paymentData.price,
      });
    } else {
      this.props.navigation.navigate("Login", {
        previousScreen: currentScreen,
      });
    }
  }

  async getSerieVideo() {
    const { serie } = this.props.route.params;
    const { currentSerie } = this.state;
    const data = await Network.getSerieVideo(
      this.context.activeCompany.id,
      serie.id,
      currentSerie.step
    );
    let step;
    if (data) {
      if (data.length == global.videoEachStep) {
        step = currentSerie.step + 1;
      } else {
        step = currentSerie.step;
      }
    }
    this.setState({
      currentSerie: { id: serie.id, data: data, step: step, loading: false },
    });
  }

  async refresh() {
    const { serie } = this.props.route.params;
    const { currentSerie } = this.state;

    this.setState({
      currentSerie: { data: [], loading: true, id: serie.id, step: 1 },
      access: serie.access == "ok" ? true : false,
      paySerie: false,
      paySubscription: false,
      bundleInfo: [],
    });
    if (serie.trailer) {
      this.getTrailer(serie.trailer);
    }

    if (serie.parent) {
      this.getBundleChild(serie.id);
    } else if (serie.access != "ok") {
      this.getSerieParent(serie.id);
    }
    if (serie && serie.access !== "ok") {
      let pSerie = false;
      let pSubscription = false;
      let subscription = {
        price: 0,
        period: 0,
      };
      if (serie.payment.price) {
        pSerie = true;
      }

      if (serie.payment.subscription.length) {
        pSubscription = true;
        subscription = {
          period: serie.payment.subscription[0].period,
          price: serie.payment.subscription[0].price,
        };
      }
      this.setState({
        currentSerie: { ...currentSerie, loading: false },
        paySerie: pSerie,
        paySubscription: pSubscription,
        subscription: subscription,
      });
    } else {
      // show serie videos
      this.getSerieVideo();
    }
  }

  viewWillFocus() {
    this.setState({ pageHasFocus: true });

    if (this.props.route.params.serie.id != this.state.currentSerie.id) {
      this.refresh();
    }
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () =>
      this.goBack()
    );
  }

  async goBack() {
    const { previousScreen } = this.props.route.params;
    const { fullscreen } = this.state;

    if (previousScreen == "SerieView") {
      // this is the case we are in a pushed SerieView
      this.props.navigation.goBack(null);
    } else {
      // this is the case this SerieView is the first
      // and we are going to the Catalog
      this.props.navigation.goBack(null);
    }
  }

  viewWillBlur() {
    this.setState({ pageHasFocus: false });

    if (this.backHandler) {
      this.backHandler.remove();
    }
  }
  focusListener: any = null;
  blurListener: any = null;

  componentWillUnmount() {
    if (this.backHandler) {
      this.backHandler.remove();
    }
    this.focusListener();
    this.blurListener();
  }

  componentWillMount() {
    this.overlayOpacity = new Animated.Value(1);
  }

  componentDidMount() {
    const { serie } = this.props.route.params;
    console.log(serie)
    this.focusListener = this.props.navigation
      .addListener("focus", () => {
        this.end = 0;
        this.viewWillFocus();
      })
      .bind(this);
    this.blurListener = this.props.navigation
      .addListener("blur", () => {
        this.end = 1;
        this.updateVideo();
        this.stopCounter();
        this.stopUpdateInterval();
        this.viewWillBlur();
      })
      .bind(this);
  }

  async openLink() {
    this.props.navigation.navigate("WebViewPage", {
      source: "https://teyuto.tv/app/paymentPreview?idc=1577&app=1",
      headers: {
        // Authorization: await Storage.getToken()
      },
    });
  }

  async fetchMore() {
    const { serie } = this.props.route.params;
    const { currentSerie } = this.state;
    if (currentSerie.data.length % global.videoEachStep == 0) {
      let data = await Network.getSerieVideo(
        this.context.activeCompany.id,
        serie.id,
        currentSerie.step
      );
      if (data && data.length) {
        let step;
        if (data.length == global.videoEachStep) {
          step = currentSerie.step + 1;
        } else {
          step = currentSerie.step;
        }
        this.setState({
          currentSerie: {
            ...currentSerie,
            data: [...currentSerie.data, ...data],
            step: step,
            loading: false,
          },
        });
      }
    }
  }

  onError(error) {
    this.setState({ errorMessage: error }, () => {
      setTimeout(() => this.setState({ errorMessage: "" }), 1800);
    });
  }

  async openSeries(item) {
    const { serie } = this.props.route.params;
    let data;
    this.setState({
      currentSerie: { ...this.state.currentSerie, loading: true },
    });
    if (serie.parent) {
      data = await Network.getSerieInfo(item.id_child);
    } else {
      data = await Network.getSerieInfo(item.id_parent);
    }
    this.setState({
      currentSerie: { ...this.state.currentSerie, loading: false },
    });
    if (data && data[0]) {
      this.props.navigation.push("SerieView", {
        serie: data[0],
        previousScreen: currentScreen,
      });
    }
  }

  async openPlayer(item) {
    const { currentSerie } = this.state;
    if (item.quiz) {
      this.onError(global.strings.QuizAlert);
    } else {
      this.context.setActualPreview(item.img_preview);
      // use the same state of the serie for a loading feedback
      this.setState({ currentSerie: { ...currentSerie, loading: true } });

      const promises = await Promise.all([
        Network.getVideoInfo(item.id),
        Network.getVideoSource(item.id),
        Network.getAttachments(item.id),
      ]);
      const videoInfo = promises[0];
      const data = promises[1];

      const attachments = promises[2];

      let video = formatVideoObject(data);

      if (!videoInfo.ondemand) {
        video.started = videoInfo.livenow;
      }

      // use the same state of the serie for a loading feedback
      this.setState({ currentSerie: { ...currentSerie, loading: false } });

      this.props.navigation.navigate("Player", {
        previousScreen: currentScreen,
        video: {
          id: item.id,
          creator: item.display_name,
          title: item.title,
          description: item.description,
          source: video,
          sourceChromecast: data.source_chromecast,
          sourceMP4: data.source_original,
          attachments: attachments,
        },

        videoInfo: {
          showLike: videoInfo.show_likes,
          showViews: videoInfo.show_views,
          likesTot: videoInfo.likes_tot,
          viewsTot: videoInfo.views,
          liked: !!videoInfo.liked,
          markers: videoInfo.markers, // banner
          productRelated: videoInfo.related_product, // prodotti correlati
        },
      });
      // this.props.navigation.navigate('Player', { activeCompany: activeCompany, previousScreen: currentScreen, video: { id: item.id, creator: item.display_name, title: item.title, description: item.description, source: video, attachments: attachments }})
    }
  }

  onProgress = (data: any) => {
    this.setState({ currentTime: data.currentTime });
  };

  onLoad = () => {
    this.setAction();
    this.counter();
    this.setUpdateInterval();
  };
  async setAction() {
    const { serie } = this.props.route.params;
    this.action = await Network.playVideo(serie.trailer, 0);
  }
  async updateVideo(time?: any) {
    const { serie } = this.props.route.params;
    await Network.updateVideo(
      serie.trailer,
      time || this.state.currentTime.toFixed(2) || 0,
      this.action,
      this.end,
      this.state.secondsPlayed
    );
  }
  counter() {
    let intervalId = setInterval(() => {
      this.setState({ secondsPlayed: this.state.secondsPlayed + 1 });
    }, 1000);
    this.setState({ intervalId: intervalId });
  }
  stopCounter() {
    clearInterval(this.state.intervalId);
    this.setState({ secondsPlayed: 0 });
  }
  setUpdateInterval() {
    let updateIntervalId = setInterval(() => {
      this.updateVideo();
      this.setState({ secondsPlayed: 0 });
    }, 20000);
    this.setState({ updateIntervalId: updateIntervalId });
  }
  stopUpdateInterval() {
    clearInterval(this.state.updateIntervalId);
  }

  render() {
    if (this.state.pageHasFocus) {
      const { serie } = this.props.route.params;
      const { currentSerie, fullscreen } = this.state;
      return (
        <Background
          header={
            <Header
              onMenuPress={() => this.props.navigation.goBack(null)}
              onLogoPress={() => this.props.navigation.popToTop()}
            />
          }
          style={{
            backgroundColor: this.context.backgroundColor,
          }}
        >
          <ScrollView
            style={{
              flex: 6,
              backgroundColor: this.context.backgroundColor,
            }}
            nestedScrollEnabled
            showsVerticalScrollIndicator={false}
          >
            <View
              style={{
                width: global.screenProp.width,
                height: (global.screenProp.width / 16) * 9,
                position: "relative",
                flexDirection: "column",
                backgroundColor: this.context.backgroundColor,

                justifyContent: "flex-end",
              }}
            >
              <OrientationLocker orientation={PORTRAIT} />
            
                <Video
                  source={{
                    uri:
                      this.state.trailerSource === null
                        ? ""
                        : this.state.trailerSource,
                  }}
                  muted={this.state.trailerVolumeMuted}
                  onProgress={this.onProgress}
                  onLoad={this.onLoad}
                  playbackRate={1}
                  repeat={true}
                  resizeMode={"cover"}
                  rate={1.0}
                  poster={serie.img_preview}
                  ignoreSilentSwitch={"ignore"}
                  ref={(ref: Video) => (this.video = ref)}
                  style={{
                    position: "absolute",
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                  }}
                />
             
              <LinearGradient
                colors={[
                  this.context.backgroundColor + "00",
                  this.context.backgroundColor + "00",
                  this.context.backgroundColor + "00",
                  this.context.backgroundColor,
                ]}
                style={{
                  width: "100%",
                  minHeight:30,
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                {this.state.trailerSource &&
                <>
                <View
                  style={{
                    padding: 7,
                  }}
                >
                  {this.state.trailerVolumeMuted ? (
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.setState({ trailerVolumeMuted: false })
                      }
                    >
                      <View>
                        <FontAwesomeIcon
                          icon={faVolumeMute}
                          size={25}
                          color={this.context.fontColor}
                        />
                      </View>
                    </TouchableWithoutFeedback>
                  ) : (
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.setState({ trailerVolumeMuted: true })
                      }
                    >
                      <View>
                        <FontAwesomeIcon
                          icon={faVolumeUp}
                          size={25}
                          color={this.context.fontColor}
                        />
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                </View>
                <View
                  style={{
                    padding: 7,
                  }}
                >
                  <TouchableWithoutFeedback
                    onPress={() => {
                      if (this.state.trailerSource) {
                        this.video.seek(0);
                      }
                    }}
                  >
                    <View>
                      <FontAwesomeIcon
                        icon={faRedo}
                        size={25}
                        color={this.context.fontColor}
                      />
                    </View>
                  </TouchableWithoutFeedback>
                  </View>
                  </>}
              </LinearGradient>
            </View>
            <View style={{ flexDirection: "column" }}>
              <View>
                <View>
                  <AppText
                    style={{
                      fontSize: 20,
                      fontWeight: "600",
                      paddingLeft: 10,
                      marginTop: 7,
                    }}
                  >
                    {serie.title}
                  </AppText>
                </View>
                <View>
                  <AppText
                    style={{
                      color: "rgb(129, 129, 130)",
                      fontSize: 16,
                      fontWeight: "300",
                      paddingLeft: 10,
                      marginBottom: 20,
                      marginTop: 20,
                      lineHeight: 21,
                    }}
                  >
                    {serie.description}
                  </AppText>
                </View>

                {serie.child && (
                  <HomePageList
                    title={global.strings.SerieIncluded}
                    data={bundleInfo}
                    onRef={(ref) => (this.onItemPress = ref)}
                    onItemPress={this.openSeries.bind(this)}
                    onEndReached={() => this.getBundleChild()}
                    onEndReachedThreshold={0.5}
                    style={{ width: "100%" }}
                  />
                )}
                {serie.parent && (
                  <HomePageList
                    title={global.strings.BundleContaining}
                    data={bundleInfo}
                    onRef={(ref) => (this.onItemPress = ref)}
                    onItemPress={this.openSeries.bind(this)}
                    style={{ width: "100%" }}
                  />
                )}
              </View>
              {serie.access == "ok" && (
                <View>
                  <FlatList
                    data={currentSerie.data}
                    style={{
                      marginBottom: 20,
                    }}
                    renderItem={({ item }) => (
                      <View style={{ marginBottom: 22 }}>
                        <Touchable
                          style={{
                            margin: 10,
                            flexDirection: "column",
                          }}
                          onPress={() => this.openPlayer(item)}
                        >
                          <ImageBackground
                            imageStyle={{ borderRadius: 5 }}
                            source={{
                              uri:
                                item.img_preview ||
                                this.context.activeCompany.default_video_image,
                            }}
                            style={[
                              styles.livePreviewStyle,
                              {
                                width: global.screenProp.width - 20,
                                height:
                                  ((global.screenProp.width - 20) / 16) * 9,
                              },
                            ]}
                          >
                            {item.duration ? (
                              <AppText style={styles.liveCollectionStyle}>
                                {item.duration}
                              </AppText>
                            ) : (
                              <AppText />
                            )}
                          </ImageBackground>
                        </Touchable>

                        <AppText style={styles.liveTitleStyle}>
                          {item.title}
                        </AppText>
                      </View>
                    )}
                    onEndReached={() => this.fetchMore()}
                    keyExtractor={(item) => String(item.id)}
                    style={styles.flex}
                    onEndReachedThreshold={0.1}
                    ListFooterComponent={<View style={{ height: 50 }} />}
                  />
                </View>
              )}

              {!fullscreen && serie.access != "ok" && (
                <View
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  {(serie.app_products && serie.app_products.product) ||
                  (serie.app_products && serie.app_products.subscription) ||
                  (serie.app_products && serie.app_products.included.length) ? (
                    <Pressable
                      onPress={() => {
                        this.props.navigation.navigate("ModalPay", {
                          _childProduct: serie.app_products.product,
                          _childSubscription: serie.app_products.subscription,
                          _parentPayments: serie.app_products.included,
                          id_series: serie.id,
                        });
                      }}
                      style={({ pressed }) => [
                        {
                          width: "85%",
                          backgroundColor: this.context.buttonColor,
                          alignItems: "center",
                          justifyContent: "center",
                          padding: 10,
                          opacity: pressed ? 0.4 : 1,
                          borderRadius: 4,
                        },
                      ]}
                    >
                      {({ pressed }) => (
                        <View
                          style={{
                            flex: 1,
                            alignItems: "center",
                            justifyContent: "space-around",
                            flexDirection: "row",
                          }}
                        >
                          <View
                            style={{
                              flex: 5,
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <Text
                              style={{
                                color: this.context.fontColor,
                                fontWeight: "bold",
                                fontSize: 24,
                              }}
                            >
                              {global.strings.AccessContent}
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <FontAwesomeIcon
                              icon={pressed ? faLockOpen : faLock}
                              size={24}
                              color={this.context.fontColor}
                            />
                          </View>
                        </View>
                      )}
                    </Pressable>
                  ) : (
                    <Text
                      style={{
                        color: this.context.fontColor,
                        fontSize: 16,
                        fontWeight: "bold",
                        paddingLeft: 10,
                      }}
                    >
                      {global.strings.Access1 +
                        this.context.activeCompany.domain +
                        global.strings.Access2}
                    </Text>
                  )}
                </View>
              )}
            </View>
          </ScrollView>
          {/* loading modal */}
          <Modal
            visible={currentSerie.loading}
            style={{
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.ModalBackground,
            }}
          >
            <PulseIndicator
              size={50}
              color={this.context.activeCompany.font_color || Colors.Text1}
            />
          </Modal>
        </Background>
      );
    } else {
      return <Background empty />;
    }
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  livePreviewStyle: {
    height: 163,
    width: 290,
    borderRadius: 5,
    backgroundColor: "black",
  },
  liveButtonStyle: {
    flex: 1,
    justifyContent: "flex-end",
  },
  liveDataContainerStyle: {
    flexWrap: "wrap",
    elevation: 1,
    marginLeft: 8,
    marginRight: 8,
  },
  liveDetailsContainerStyle: {
    marginRight: 4,
    justifyContent: "center",
  },
  liveTitleStyle: {
    fontWeight: "bold",
    fontSize: 15,
    marginTop: 4,
    marginLeft: 10,
    opacity: 0.7,
  },
  liveCollectionStyle: {
    fontWeight: "700",
    padding: 4,
    backgroundColor: "rgb(48,48,48)",
    alignSelf: "flex-end",
    marginTop: 3,
    marginRight: 3,
    fontSize: 14,
    marginBottom: 10,
    color: "white",
  },
  liveDateStyle: {
    color: Colors.PlaceholderText,
    marginBottom: 2,
    fontSize: 14,
  },
  listFooterStyle: {
    height: 80,
  },
  noLiveTextStyle: {
    color: Colors.Text1,
    marginLeft: 3,
    fontSize: 16,
  },
  spinnerContainerStyle: {
    justifyContent: "center",
    alignItems: "center",
  },

  boxTitleStyle: {
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "rgb(48,48,48)",
    justifyContent: "center",
  },

  // footer
  buttonIconContainerStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonGradientStyle: {
    height: 80,
    width: 80,
    alignSelf: "center",
    borderRadius: 50,
    elevation: 1,
  },
  buttonIconStyle: {
    fontWeight: "600",
    alignSelf: "center",
  },
  buttonStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  translucentStyle: {
    flex: 1,
    justifyContent: "flex-end",
    borderRadius: 5,
    padding: 8,
  },

  imgStyle: {
    height: 50,
    width: 50,
    alignSelf: "center",
    margin: 5,
  },

  // modal
  modalWindowContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  modalWindowStyle: {
    backgroundColor: Colors.Text1,
    borderColor: Colors.Border1,
    borderRadius: 7,
    elevation: 1,
  },
  modalQuestionContainerStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  modalQuestionStyle: {
    fontSize: 18,
  },
  modalAnswerContainerStyle: {
    alignSelf: "flex-end",
    flexDirection: "row",
  },
  modalAnswerTextStyle: {
    borderColor: Colors.Border1,
    width: 150,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
  },
  paymentInput: {
    width: 150,
    height: 40,
    color: Colors.Text1,
    // backgroundColor: 'red',
    fontSize: 18,
    marginTop: Platform.OS == "ios" ? 8 : 0,
  },

  trailerTitleStyle: {
    textAlignVertical: "center",
    color: "white",
    padding: 7,
    fontWeight: "bold",
    fontSize: 17,
    marginRight: 10,
    textAlign: "center",
  },

  iconaPlayStyle: {
    textAlign: "center",
    marginLeft: 10,
    opacity: 0.8,
  },
});

export { SerieView };
