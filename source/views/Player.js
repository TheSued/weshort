import React from "react";
import Client from "./Client";

import {
  View,
  ScrollView,
  StyleSheet,
  Animated,
  Keyboard,
  FlatList,
  Text,
  Image,
  Platform,
  BackHandler,
  TVEventHandler,
  TouchableWithoutFeedback,
} from "react-native";
import Orientation from "react-native-orientation-locker";
import Icon from "react-native-vector-icons/MaterialIcons";
import Pusher from "pusher-js/react-native";
import { check, request, PERMISSIONS, RESULTS } from "react-native-permissions";
import RNFetchBlob from "rn-fetch-blob";
import GoogleCast, { CastContext, CastButton } from "react-native-google-cast";

import Routes from "../environments/Routes";
import Colors from "../environments/Colors";
import * as Network from "../network";
import { ActiveCompanyContext } from "../providers/CompanyProvider";
import { Header, Toast, VideoPlayer } from "../components";
import { Background, Input, Rotate, Touchable } from "../components/common";

const currentScreen = "Player";

function MyComponent(color) {
  // This will automatically rerender when client is connected to a device
  // (after pressing the button that's rendered below)

  return (
    <CastButton
      style={{
        tintColor: color,
        width: 40,
        height: 40,
      }}
    />
  );
}
class Player extends React.Component {
  static contextType = ActiveCompanyContext;

  videoPlayer;

  state = {
    loadingUnderVideroArea: true,
    id: "",
    showDescription: false,
    fullscreen: false,
    viewers: 0,
    chat: [],
    message: "",
    errorMessage: "",
    attachments: [],
    storagePermissions: false,
    castConnected: false,
    castModal: false,
    currentTime: 0,
    streamVideoTime: 0,
    started: false,
    isTv: false,
    pageHasFocus: false,
    baseLike: 0,
    editLikeStatus: false,
    comment: [],
    activeComment: false,
    step: 1,
    viewsTot: 0,
    underVideoArea: "flex",
    elementPreviewMarker: [],
    elementShowPreview: [],
    relatedProducts: [],
    relatedPreviewProducts: [],
    currentBanner: [],
  };

  componentWillUnmount() {
    CastContext.onCastStateChanged().remove();

    Orientation.removeAllListeners();
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();

    if (this.channelLive) {
      this.channelLive.unsubscribe();
    }
    if (this.socket && this.channel) {
      this.channel.unbind_global();
      this.socket.unsubscribe();
    }
    if (this.backHandler) {
      this.backHandler.remove();
    }
    this.focusListener();
    this.blurListener();
  }

  componentDidMount() {
    CastContext.onCastStateChanged((castState) => {
      if (castState === "connected" || castState === "connecting") {
        this.openCastController();
      }
    });
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
    this.focusListener = this.props.navigation.addListener("focus", () =>
      this.viewWillFocus()
    );
    this.blurListener = this.props.navigation.addListener("blur", () =>
      this.viewWillBlur()
    );
    this.getAds();
    this.showComments();
  }
  _keyboardDidShow = () => {
    this.setState({ underVideoArea: "none" });
  };

  _keyboardDidHide = () => {
    this.setState({ underVideoArea: "flex" });
  };
  Switch() {}

  async goBack() {
    const { previousScreen } = this.props.route.params;
    const { fullscreen } = this.state;

    if (fullscreen && !Platform.isTV) {
      this.toggleFullscreen();
    } else {
      if (previousScreen === "SerieView") {
        // this is the case we are in a pushed SerieView
        this.props.navigation.pop();
      } else {
        // this is the case this SerieView is the first
        // and we are going to the Catalog
        this.props.navigation.popToTop();
        //this.props.navigation.goBack();
      }
    }
  }

  onCurrentTime = (time) => {
    this.setState({ streamVideoTime: time.toFixed(2) });
  };

  getBannerPopUp() {
    const { streamVideoTime } = this.state;
    const banner = this.state.elementShowPreview
      .filter(
        (el) => streamVideoTime >= +el.time && streamVideoTime <= +el.time + 4
      )
      .map((el) => {
        if (!!el.title === true || !!el.url === true) {
          if (el.show_preview === "1") {
            return (
              <View style={[styles.videoBarStyle]}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.context.setPlayerCurrentTime(
                      this.state.streamVideoTime
                    );
                    this.openLink(el.url);
                  }}
                >
                  <View style={{ flex: 1, flexDirection: "row", padding: 8 }}>
                    <View
                      style={{
                        flex: 2,
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        alignItems: "center",
                      }}
                    >
                      <Image
                        resizeMode={"contain"}
                        source={{ uri: el.img }}
                        style={styles.imgStyle}
                      />
                    </View>

                    <View
                      style={{
                        flex: 4,
                        flexDirection: "column",
                        justifyContent: "space-between",
                      }}
                    >
                      <View>
                        <Text
                          style={{
                            color: this.context.fontColor,
                            fontWeight: "bold",
                            fontSize: 10,
                          }}
                        >
                          {el.title.length > 23
                            ? el.title.substr(0, 20) + "..."
                            : el.title}
                        </Text>
                      </View>
                      <View>
                        <Text
                          style={{
                            color: this.context.fontColor,
                            fontWeight: "bold",
                            fontSize: 10,
                          }}
                        >
                          {el.price} {el.currency}
                        </Text>
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            );
          }
          if (
            el.type === "text" &&
            el.title !== undefined &&
            el.url !== undefined
          ) {
            return (
              <View
                style={[
                  {
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  },
                  styles.videoBarStyle,
                ]}
              >
                <Text style={{ color: this.context.fontColor }}>
                  {el.title}
                </Text>
              </View>
            );
          }
          return (
            <Touchable
              onPress={() => this.openLink(el.url)}
              style={[
                {
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                },
                styles.videoBarStyle,
              ]}
            >
              <Text style={{ color: this.context.fontColor, width: 220 }}>
                {el.url}
              </Text>
            </Touchable>
          );
        }
      });
    return banner;
  }

  openLink(externalUrl) {
    this.props.navigation.navigate("WebViewPage", {
      source: externalUrl,
    });
  }
  openCastController() {
    const { video } = this.props.route.params;
    this.props.navigation.navigate("CastPage", {
      video: video.sourceChromecast,
      videoId: video.id,
    });
  }
  async getChat(videoId) {
    let live = [];
    let qea = [];
    if (this.context.activeCompany.video_chat) {
      live = await Network.getChat(videoId);
    }
    if (this.context.activeCompany.video_qea) {
      qea = await Network.getQeA(videoId);
    }
    qea.forEach((c) => (c.avatar = c.profile_image));

    let chat = [];

    if (live.length) {
      chat = [...chat, ...live];
    }
    if (qea.length) {
      chat = [...chat, ...qea];
    }
    return chat;
  }

  async getComment() {
    const { video } = this.props.route.params;
    const { comment } = this.state;
    let step = this.state.step;

    const promises = await Network.getCommentQeA(video.id, 1);
    if (promises.length >= global.videoEachStep) {
      step += 1;
    }
    if (promises.length) {
      this.setState({ comment: [...comment, ...promises], step });
    }
  }

  async getMoreComment() {
    const { video } = this.props.route.params;
    const { comment } = this.state;
    let step = this.state.step;

    if (comment.length % global.videoEachStep === 0) {
      const responseMess = await Network.getCommentQeA(video.id, step);
      if (responseMess.length >= global.videoEachStep) {
        step += 1;
      }
      this.setState({ comment: [...comment, ...responseMess], step });
    }
  }

  addComment() {
    const { message, comment } = this.state;
    if (comment.length % global.videoEachStep === 0) {
      comment.splice(comment.length - 1, 1);
    }
    const commentToAdd = {
      question: message,
      user: this.context.user.displayName,
      profile_image: this.context.user.profileImage,
      date: new Date().toISOString().replace(/T/, " ").replace(/\..+/, ""),
    };
    this.setState({ comment: [commentToAdd, ...comment] });
  }

  showComments() {
    this.setState({ activeComment: !this.state.activeComment });
    if (!this.state.activeComment && this.state.step === 1) {
      this.getComment();
    }
  }

  async viewWillFocus() {
    const sessionManager = GoogleCast.getSessionManager();
    const currentCastSession = sessionManager
      .getCurrentCastSession()
      .then((response) => {
        console.log(response, "cast response player");
        response === null ? false : this.openCastController();
      });

    const { video } = this.props.route.params;

    this.setState({ pageHasFocus: true });

    if (!Platform.isTV) {
      Orientation.addOrientationListener(this._orientationDidChange);
      Orientation.unlockAllOrientations();
    }

    if (video.id != this.state.id) {
      this.setState({
        id: video.id,
        chat: [],
        viewers: 0,
        message: "",
        attachments: [],
        fullscreen: Platform.isTV,
        isTV: Platform.isTV,
      });

      if (video.source.live) {
        this.setLiveListener(video.id);
        this.setState({
          uri: video.source.hls || null,
          started: video.source.started,
        });

        if (!this.socket) {
          this.socket = new Pusher("1951c674aef8e8b86430", {
            cluster: "eu",
          });
        }
        if (!this.channel) {
          this.channel = this.socket.subscribe("teyuto_live_chat_" + video.id);
          this.channel.bind("live_chat", (data) => {
            let dataParse = JSON.parse(data);
            this.setState({ chat: [...this.state.chat, ...dataParse] }, () => {
              if (this.chat !== null) {
                this.chat.scrollToEnd();
              }
            });
          });
        }
      } else {
        const { fullhd, hd, hq, original } = video.source;

        const started = video.source.started;
        const uri = fullhd || hd || hq || original;

        const viewers = this.context.activeCompany.show_video_views
          ? await this.getViewers()
          : 0;
        const chat = await this.getChat(video.id);
        this.setState({ started, uri, chat, viewers });
      }
    }

    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () =>
      this.goBack()
    );
    const attachments = this.setAttachmentsList();
    this.setState({ attachments });
    this.initLike();
  }

  setLiveListener(videoId) {
    if (!this.socket) {
      this.socket = new Pusher("1951c674aef8e8b86430", {
        cluster: "eu",
      });
    }
    if (!this.channelLive) {
      this.channelLive = this.socket.subscribe("live_" + videoId);
    }
    this.channelLive.bind("notification", (data) => {
      const dataParse = JSON.parse(data);
      switch (dataParse.type) {
        case "live_start":
          setTimeout(() => this.setState({ started: true }), dataParse.offset);
          break;
        case "currently_users":
          if (this.context.activeCompany.show_live_views) {
            this.setState({ viewers: dataParse.val });
          }
          break;
      }
    });
  }

  setAttachmentsList() {
    const { video } = this.props.route.params;

    // setup attachments lists
    if (video.attachments.length) {
      let listsLength = 0;
      video.attachments.forEach((item) => {
        listsLength = listsLength + item.chars;
      });
      listsLength = Math.ceil(listsLength / 2);
      let tmp_array = video.attachments;
      let tmp_longerIndex = 0;
      let list = { row1: [], row2: [] };
      let l_r1 = 0,
        l_r2 = 0;
      while (tmp_array.length != 0) {
        tmp_longerIndex = this.getLonger(tmp_array);
        if (l_r1 <= l_r2) {
          list = {
            row1: [...list.row1, tmp_array[tmp_longerIndex]],
            row2: [...list.row2],
          };
          l_r1 = l_r1 + tmp_array[tmp_longerIndex].name.length;
        } else {
          list = {
            row1: [...list.row1],
            row2: [...list.row2, tmp_array[tmp_longerIndex]],
          };
          l_r2 = l_r2 + tmp_array[tmp_longerIndex].name.length;
        }
        tmp_array.splice(tmp_longerIndex, 1);
      }
      return list;
      // this.setState({ attachments: list });
    }
  }

  // needed from attachments setup -- return the index of the longer string
  getLonger(array) {
    let longer = 0;
    for (let i = 0; i < array.length; i++) {
      if (array[longer].name.length < array[i].name.length) {
        longer = i;
      }
    }
    return longer;
  }

  enableTVEventHandler() {
    if (!this.tvEventHandler) {
      this.tvEventHandler = new TVEventHandler();
      this.tvEventHandler.enable(this, function (cmp, evt) {
        if (evt && evt.eventType === "right") {
          cmp.videoPlayer.skipForward();
        } else if (evt && evt.eventType === "up") {
          cmp.goBack();
        } else if (evt && evt.eventType === "left") {
          cmp.videoPlayer.skipBackward();
        } else if (evt && evt.eventType === "down") {
        } else if (evt && evt.eventType === "select") {
          if (!cmp.playPauseTimer) {
            cmp.playPauseTimer = setTimeout(() => {
              cmp.playPauseTimer = false;
            }, 300);
            cmp.videoPlayer.togglePlayer();
          }
        }
      });
    }
  }

  disableTVEventHandler() {
    if (this.tvEventHandler) {
      this.tvEventHandler.disable();
      delete this.tvEventHandler;
    }
  }

  viewWillBlur() {
    this.setState({ pageHasFocus: false });
    this.disableTVEventHandler();
    this.videoPlayer.setState({ paused: true, fullscreen: false });
    if (this.backHandler) {
      this.backHandler.remove();
    }
    Orientation.removeAllListeners();
    if (this.socket && this.channel) {
      this.channel.unbind_global();
      this.socket.unsubscribe();
    }
  }

  async checkStoragePermissions() {
    if (Platform.OS == "android") {
      try {
        let response = 0;
        response = await check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
        if (response != RESULTS.GRANTED) {
          let permissions = await request(
            PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE
          );
          if (permissions == RESULTS.GRANTED) {
            this.setState({ storagePermissions: true });
            return true;
          } else {
            this.setState({ storagePermissions: false });
            return false;
          }
        } else {
          this.setState({ storagePermissions: true });
          return true;
        }
      } catch (error) {
        this.setState({ storagePermissions: false });
        return false;
      }
    } else {
      // storage permissions don't exist on iOS
      this.setState({ storagePermissions: true });
      return true;
    }
  }

  componentWillMount() {
    this.overlayOpacity = new Animated.Value(1);
  }

  _orientationDidChange = (orientation) => {
    if (orientation == "LANDSCAPE-RIGHT" || orientation == "LANDSCAPE-LEFT") {
      this.setState({ fullscreen: true });
    } else if (orientation == "PORTRAIT") {
      this.setState({ fullscreen: false });
    }
  };

  async getViewers() {
    const { video } = this.props.route.params;

    const data = await Network.getViewers(video.id);
    return data || 0;
  }

  toggleFullscreen() {
    if (this.state.fullscreen) {
      this.setState({ fullscreen: false }, () => {
        Orientation.lockToPortrait();
        setTimeout(() => {
          Orientation.unlockAllOrientations();
        }, 5000);
      });
    } else {
      this.setState({ fullscreen: true }, () => {
        Orientation.lockToLandscape();
      });
    }
  }

  async getAds() {
    //Video info arrivano dai params
    const { videoInfo } = this.props.route.params;
    if (!videoInfo) {
      return false;
    }
    //due array che contengono i risultati delle call dei show preview per banner e prodotti correlati
    const bannerPromises = [];
    const productPromises = [];
    let numberOfBanners = 0;
    let numberOfProducts = 0;
    //per ogni marker fa una call con marker.val="url" e pusha il risultato in banner e in products.

    for (const marker of videoInfo.markers) {
      numberOfBanners++;

      if (marker.show_preview === "1") {
        bannerPromises.push(Network.GetPreviewUrl(marker.val));
      } else {
        bannerPromises.push(marker);
      }
    }
    for (const product of videoInfo.productRelated) {
      numberOfProducts++;

      productPromises.push(Network.GetPreviewUrl(product));
    }
    //attendiamo il load di tutti i results per procedere

    const results = await Promise.all([...bannerPromises, ...productPromises]);

    const bannerResponses = results.slice(0, numberOfBanners);
    const productResponses = results.slice(numberOfBanners);

    const productResult = productResponses
      .filter((product) => product.status !== "error")
      .map((product) => ({
        title: product.title,
        url: product.url,
        img: product.img,
        time: product.time,
        type: product.type,
        price: product.price,
        currency: product.currency,
      }));

    const bannerResult = [];
    let lastIndex = 0;
    videoInfo.markers.forEach((marker, index) => {
      if (marker.show_preview === "1") {
        bannerResult.push({
          title: bannerResponses[lastIndex].title,
          url: bannerResponses[lastIndex].url,
          img: bannerResponses[lastIndex].img,
          time: marker.time,
          type: marker.type,
          price: bannerResponses[lastIndex].price,
          currency: bannerResponses[lastIndex].currency,
          show_preview: marker.show_preview,
        });
        lastIndex++;
      } else {
        bannerResult.push({
          title: marker.text,
          url: marker.val,
          time: marker.time,
          type: marker.type,
          show_preview: marker.show_preview,
        });
      }
    });

    this.setState({
      relatedPreviewProducts: productResult,
      elementShowPreview: bannerResult,
    });
  }
  navigateToPaymentSerie(paymentData) {
    const { serie } = this.props.route.params;
    if (this.context.user) {
      this.props.navigation.navigate("PaymentSerie", {
        serie: serie,
        previousScreen: currentScreen,
        payment: paymentData.period,
        price: paymentData.price,
      });
    } else {
      this.props.navigation.navigate("Login", {
        previousScreen: currentScreen,
      });
    }
  }

  initLike() {
    const { videoInfo } = this.props.route.params;
    this.setState({
      baseLike: videoInfo.likesTot,
      editLikeStatus: videoInfo.liked,
    });
  }

  async updateLike() {
    //updateLike
    const { video } = this.props.route.params;
    const newLike = await Network.addLike(video.id);
    const editLikeStatus = newLike.status === "likes_add";
    const editToTLikes = newLike["tot_likes"];

    this.setState({ baseLike: editToTLikes, editLikeStatus });
  }

  renderHeader() {
    if (this.state.fullscreen) {
      return null;
    } else {
      return (
        <Header
          onCameraPress={() =>
            this.props.navigation.navigate("ManageLive", {
              previousScreen: currentScreen,
            })
          }
          onMenuPress={() => {
            this.context.setPlayerCurrentTime(0);
            this.goBack();
          }}
          onLogoPress={() => {
            this.context.setPlayerCurrentTime(0);
            this.props.navigation.popToTop();
          }}
        />
      );
    }
  }

  renderChat() {
    const { video, videoInfo } = this.props.route.params;
    const { showDescription, fullscreen, chat } = this.state;
    if (!showDescription && !fullscreen) {
      if (video.source.live && this.context.activeCompany.video_chat) {
        return (
          <FlatList
            ref={(ref) => {
              this.chat = ref;
            }}
            ListEmptyComponent={<View style={{ flex: 1, marginTop: 10 }} />}
            showsVerticalScrollIndicator={false}
            data={chat}
            renderItem={({ item }) => (
              <View
                style={{
                  marginTop: 10,
                  flex: 1,
                }}
              >
                <View style={{ flexDirection: "row" }}>
                  <Image
                    source={{ uri: item.avatar || Routes.NoProfileImg }}
                    style={{
                      height: 28,
                      width: 28,
                      borderRadius: 14,
                      marginRight: 4,
                    }}
                  />
                  <Text
                    style={{
                      fontWeight: "600",
                      fontSize: 18,
                      color: this.context.fontColor,
                    }}
                  >
                    {item.user}
                  </Text>
                </View>
                <Text
                  style={{
                    color: this.context.fontColor,
                    marginLeft: 8,
                    marginBottom: 12,
                    fontSize: 16,
                  }}
                  multiline
                  autogrow
                  maxHeight={130}
                >
                  {item.message}
                </Text>
              </View>
            )}
            keyExtractor={(item) => item.idChat}
            style={styles.listStyle}
          />
        );
      } else if (this.context.activeCompany.video_qea) {
        return (
          <FlatList
            ref={(ref) => {
              this.chat = ref;
            }}
            ListEmptyComponent={<View style={{ flex: 1 }} />}
            showsVerticalScrollIndicator={false}
            onContentSizeChange={() => this.chat.scrollToEnd()}
            data={chat}
            renderItem={({ item }) => (
              <Touchable
                onPress={() => {
                  item.message
                    ? this.videoPlayer.seek(parseInt(item.time))
                    : null;
                }}
              >
                <View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Image
                      source={{ uri: item.avatar || Routes.NoProfileImg }}
                      style={{ height: 28, width: 28, borderRadius: 14 }}
                    />
                    <Text
                      style={{
                        fontWeight: "600",
                        fontSize: 18,
                        color: item.message
                          ? item.nameColor == "white08"
                            ? Colors.White08
                            : Colors.Secondary1
                          : item.nameColor,
                      }}
                    >
                      {item.user}
                    </Text>
                    {item.message && (
                      <Text
                        style={{ fontSize: 16, color: Colors.PlaceholderText }}
                      >
                        {global.strings.AnswerAt1 +
                          item.time +
                          global.strings.AnswerAt2}
                      </Text>
                    )}
                  </View>
                  {item.question && (
                    <View>
                      <View
                        style={{ flexDirection: "row", alignItems: "center" }}
                      >
                        <Text
                          style={{
                            marginLeft: 3,
                            marginRight: 3,
                            marginBottom: 12,
                            fontSize: 20,
                            color: Colors.PlaceholderText,
                          }}
                        >
                          {global.strings.Q}
                        </Text>
                        <Text
                          style={{
                            marginRight: 3,
                            marginBottom: 12,
                            fontSize: 16,
                          }}
                          multiline
                          autogrow
                          ellipsizeMode={"clip"}
                        >
                          {item.question}
                        </Text>
                      </View>
                      <View
                        style={{ flexDirection: "row", alignItems: "center" }}
                      >
                        <Text
                          style={{
                            marginLeft: 3,
                            marginRight: 3,
                            marginBottom: 12,
                            fontSize: 20,
                            color: Colors.PlaceholderText,
                          }}
                        >
                          {global.strings.A}
                        </Text>
                        <Text
                          style={{
                            marginRight: 3,
                            marginBottom: 12,
                            fontSize: 16,
                          }}
                          multiline
                          autogrow
                          ellipsizeMode={"clip"}
                        >
                          {item.answer}
                        </Text>
                      </View>
                    </View>
                  )}
                  {item.message && (
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          marginRight: 3,
                          marginBottom: 12,
                          fontSize: 16,
                        }}
                        multiline
                        autogrow
                      >
                        {item.message}
                      </Text>
                    </View>
                  )}
                </View>
              </Touchable>
            )}
            keyExtractor={(item) => String(item.id) || String(item.idChat)}
            style={styles.listStyle}
          />
        );
      } else {
        return (
          <Text
            style={{
              color: this.context.fontColor,
              fontSize: 16,
              marginLeft: 8,
            }}
          >
            {video.description}
          </Text>
        );
      }
    }
  }

  async sendMessage() {
    const { live } = this.props.route.params.video.source;
    const { message, id, currentTime, InfoUserData } = this.state;

    if (message !== "") {
      if (live) {
        Network.sendMessage(
          id,
          currentTime,
          this.context.user.displayName,
          message,
          this.context.user.profileImage
        );
      } else {
        const response = await Network.sendQeA(id, message);
        if (response) {
          this.setState({ errorMessage: global.strings.QuestionSent }, () =>
            setTimeout(() => this.setState({ errorMessage: "" }), 3000)
          );
          this.addComment();
        } else {
          this.setState(
            { errorMessage: global.strings.GeneralErrorShort },
            () => setTimeout(() => this.setState({ errorMessage: "" }), 3000)
          );
        }
      }

      this.setState({ message: "" });
      Keyboard.dismiss();
    }
  }

  async downloadAttachment(row, item, index) {
    const { attachments } = this.state;
    const { config, fs } = RNFetchBlob;

    // check permissions -- return always true if iOS
    const permissions = await this.checkStoragePermissions();

    if (permissions) {
      let dir =
        Platform.OS === "ios" ? fs.dirs.DocumentDir : fs.dirs.DownloadDir;

      let path = dir + "/" + item.name + "." + item.extension;
      try {
        // check if file exists -- use numbers to not replace the file
        let exist = await fs.exists(path);
        if (exist) {
          for (let i = 1; i < 100; i++) {
            let check = dir + "/" + item.name + "(" + i + ")." + item.extension;
            exist = await fs.exists(check);
            if (!exist) {
              path = check;
              break;
            }
          }
        }
      } catch (error) {
        console.log(
          "downloadAttachmentError: failed to check file exist for duplicate names loop"
        );
      }

      let options = {
        path: path,
        fileCache: true,
        addAndroidDownloads: {
          useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
          notification: false,
          path: path, // this is the path where your downloaded file will live in
          description: "Downloading file...",
        },
      };

      try {
        let response = await config(options).fetch("GET", item.file);

        if (response) {
          if (row == 1) {
            tmp_array = attachments.row1;
            tmp_array[index].downloaded = true;
            this.setState({
              attachments: { row1: tmp_array, row2: attachments.row2 },
            });
          } else {
            tmp_array = attachments.row2;
            tmp_array[index].downloaded = true;
            this.setState({
              attachments: { row1: attachments.row1, row2: tmp_array },
            });
          }
          this.setState(
            { errorMessage: global.strings.DownloadSuccessfull },
            () => setTimeout(() => this.setState({ errorMessage: "" }), 3000)
          );
        } else {
          // server error
          console.log("downloadError: bad server response", response);
          this.setState({ errorMessage: global.strings.GeneralError }, () =>
            setTimeout(() => this.setState({ errorMessage: "" }), 3000)
          );
        }
      } catch (error) {
        // RNFetchBlob error
        console.log("downloadError: ", error);
        this.setState({ errorMessage: global.strings.GeneralError }, () =>
          setTimeout(() => this.setState({ errorMessage: "" }), 3000)
        );
      }
    } else {
      // error no permissions
      this.setState(
        { errorMessage: global.strings.StoragePermissionsError },
        () => setTimeout(() => this.setState({ errorMessage: "" }), 3000)
      );
    }
  }

  onCurrentTime = (time) => {
    this.setState({ streamVideoTime: time.toFixed(2) });
  };

  render() {
    if (this.state.pageHasFocus) {
      const { video, videoInfo } = this.props.route.params;
      const {
        attachments,
        castConnected,
        castModal,
        baseLike,
        editLikeStatus,
        comment,
        activeComment,
        message,
        InfoUserData,
      } = this.state;
      this.Switch();
      return (
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}
        >
          <Background
            simple
            fullscreen={this.state.fullscreen}
            header={this.renderHeader()}
            onScrolldown
          >
            {/* test in progress */}

            <VideoPlayer
              id={video.id}
              title={video.title}
              uri={this.state.uri}
              uriMP4={video.sourceMP4}
              live={video.source.live}
              started={this.state.started}
              fullscreen={this.state.fullscreen}
              viewers={this.state.viewers}
              fullscreenController={() => this.toggleFullscreen()}
              onBack={() => this.goBack()}
              ref={(ref) => {
                this.videoPlayer = ref;
              }}
              onProgressTime={(time) => this.onCurrentTime(time)}
            />

            {this.getBannerPopUp()}
            {!this.state.fullscreen && (
              <TouchableWithoutFeedback
                onPress={() => {
                  Keyboard.dismiss();
                }}
              >
                <View style={{ padding: 10 }}>
                  <View style={{ flexDirection: "column-reverse" }}>
                    <View
                      style={{ flexDirection: "row", marginTop: 10 }}
                      onPress={() => Keyboard.dismiss()}
                    >
                      <Text
                        style={{ fontSize: 18, color: this.context.fontColor }}
                      >
                        {video.title}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Text
                        style={{ color: Colors.PlaceholderText, fontSize: 16 }}
                      >
                        {video.attachments.length ? (
                          <Icon
                            name="attach-file"
                            size={23}
                            color={Colors.PlaceholderText}
                          />
                        ) : (
                          ""
                        )}
                      </Text>
                      {/* {(this.context.activeCompany.video_qea ||
                      this.context.activeCompany.video_chat) && (
                      <Rotate
                        params={{ start: "0deg", end: "180deg" }}
                        controller={this.state.showDescription}
                        style={styles.descriptionIconStyle}
                      >
                        <Icon
                          name="arrow-drop-down"
                          size={23}
                          color={Colors.PlaceholderText}
                          onPress={() =>
                            this.setState({
                              showDescription: !this.state.showDescription,
                            })
                          }
                        />
                      </Rotate>
                    )} */}

                      {MyComponent(this.context.fontColor)}

                      {videoInfo && videoInfo.showLike && (
                        <Touchable
                          onPress={() => this.updateLike(video.id)}
                          style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            alignSelf: "center",
                            marginLeft: 15,
                          }}
                        >
                          {editLikeStatus && (
                            <Icon name="favorite" size={21} color="red" />
                          )}
                          {!editLikeStatus && (
                            <Icon
                              name="favorite"
                              size={21}
                              color="rgb(129, 129, 130)"
                            />
                          )}

                          <Text style={{ marginLeft: 5 }}>{baseLike}</Text>
                        </Touchable>
                      )}

                      {videoInfo && videoInfo.showViews && (
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            alignSelf: "center",
                            marginLeft: 10,
                          }}
                        >
                          <Icon
                            name="visibility"
                            size={21}
                            color="rgb(129, 129, 130)"
                          />
                          <Text style={{ marginLeft: 5 }}>
                            {videoInfo.viewsTot}
                          </Text>
                        </View>
                      )}
                    </View>
                  </View>
                  {/* <Text style={{ color: this.context.fontColor, fontSize: 16 }}>
                    {video.creator}
                  </Text> */}

                  {this.state.showDescription && (
                    <View>
                      {attachments && (
                        <ScrollView
                          horizontal
                          contentContainerStyle={{ flexDirection: "column" }}
                        >
                          <View style={{ flexDirection: "row" }}>
                            {attachments.row1 &&
                              attachments.row1.map((item, index) => {
                                return (
                                  <Touchable
                                    key={index}
                                    style={{
                                      margin: 3,
                                      padding: 4,
                                      paddingHorizontal: 8,
                                      borderRadius: 5,
                                      backgroundColor: item.downloaded
                                        ? Colors.PlaceholderText
                                        : Colors.Secondary2,
                                    }}
                                    onPress={() =>
                                      this.downloadAttachment(1, item, index)
                                    }
                                  >
                                    <Text
                                      style={{
                                        fontSize: 16,
                                        color: this.context.fontColor,
                                        fontWeight: "300",
                                      }}
                                    >
                                      {item.extension}
                                      <Text
                                        style={{
                                          fontSize: 16,
                                          fontWeight: "300",
                                        }}
                                      >
                                        {item.name}
                                      </Text>
                                    </Text>
                                  </Touchable>
                                );
                              })}
                          </View>
                          <View style={{ flexDirection: "row" }}>
                            {attachments.row2 &&
                              attachments.row2.map((item, index) => {
                                return (
                                  <Touchable
                                    style={{
                                      margin: 3,
                                      padding: 4,
                                      paddingHorizontal: 8,
                                      borderRadius: 5,
                                      backgroundColor: item.downloaded
                                        ? Colors.PlaceholderText
                                        : Colors.Secondary2,
                                    }}
                                    key={index}
                                    onPress={() =>
                                      this.downloadAttachment(2, item, index)
                                    }
                                  >
                                    <Text
                                      style={{
                                        fontSize: 16,
                                        color: this.context.fontColor,
                                        fontWeight: "300",
                                      }}
                                    >
                                      {item.extension}
                                      <Text
                                        style={{
                                          fontSize: 16,
                                          fontWeight: "300",
                                        }}
                                      >
                                        {item.name}
                                      </Text>
                                    </Text>
                                  </Touchable>
                                );
                              })}
                          </View>
                        </ScrollView>
                      )}
                      <Text
                        style={{ color: this.context.fontColor, fontSize: 16 }}
                      >
                        {video.description}
                      </Text>
                    </View>
                  )}
                </View>
              </TouchableWithoutFeedback>
            )}
            {/* <Client video={this.props.route.params.video.sourceChromecast} /> */}
            <View
              style={{
                display: `${this.state.underVideoArea}`,
                flexDirection: "column",
                flex: 6,
              }}
            >
              {video.source.live ? (
                this.renderChat()
              ) : (
                <>
                  <FlatList
                    style={{ flex: 4, paddingBottom: 40 }}
                    data={this.state.relatedPreviewProducts}
                    horizontal
                    renderItem={({ item }) => (
                      <Touchable
                        style={{
                          flexDirection: "row",
                          width: 100,
                          height: 150,
                          marginRight: 50,
                        }}
                        onPress={() => {
                          this.context.setPlayerCurrentTime(
                            this.state.streamVideoTime
                          );
                          this.openLink(item.url);
                        }}
                      >
                        {!Platform.isTV && (
                          <View
                            style={{
                              marginLeft: 20,
                              flexDirection: "column",
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center",
                              }}
                            >
                              <Image
                                resizeMode={"contain"}
                                source={{ uri: item.img }}
                                style={{ width: 90, height: 90 }}
                              />
                            </View>
                            <View
                              style={{
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center",
                              }}
                            >
                              <Text>
                                {item.title.length > 13
                                  ? item.title.substr(0, 9) + "..."
                                  : item.title}
                              </Text>
                            </View>
                            <View
                              style={{
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center",
                              }}
                            >
                              <Text>
                                {item.price} {item.currency}
                              </Text>
                            </View>
                          </View>
                        )}
                      </Touchable>
                    )}
                    keyExtractor={(item, index) => String(index)}
                  />
                  <FlatList
                    data={this.state.comment}
                    style={{ flex: 2 }}
                    renderItem={({ item }) => (
                      <>
                        <View
                          style={{
                            marginTop: 7,
                            flexDirection: "row",
                          }}
                        >
                          {/* <View style={{ flex: 1 }}>
                      <Image
                        resizeMode={"contain"}
                        source={{
                          uri: item.profile_image || Routes.NoProfileImg,
                        }}
                        style={styles.imgStyle}
                      />
                    </View> */}
                          <View
                            style={{
                              flex: 6,
                              paddingHorizontal: 10,
                              flexDirection: "column",
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row",
                                alignItems: "center",
                              }}
                            >
                              <Text
                                style={{
                                  color: this.context.fontColor,
                                  fontSize: 10,
                                  fontWeight: "bold",
                                }}
                              >
                                {item.user}
                              </Text>
                              <Text
                                style={{
                                  color: "rgb(129, 129, 130)",
                                  fontSize: 9,
                                  marginLeft: 10,
                                }}
                              >
                                {item.date}
                              </Text>
                            </View>
                            <View>
                              <Text color={this.context.fontColor}>
                                {item.question}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </>
                    )}
                    onEndReachedThreshold={0.2}
                    onEndReached={() => this.getMoreComment()}
                  />
                </>
              )}
            </View>

            {!this.state.fullscreen &&
              (this.context.activeCompany.video_qea ||
                this.context.activeCompany.video_chat) && (
                <View
                  style={{
                    flexDirection: "row",
                    marginHorizontal: 3,
                    alignItems: "center",
                  }}
                >
                  <Input
                    color={this.context.fontColor}
                    multiline
                    autogrow
                    maxHeight={100}
                    autoCorrect={false}
                    onChangeText={(text) => this.setState({ message: text })}
                    value={this.state.message}
                    placeholder={
                      video.source.live
                        ? global.strings.SaySomething
                        : global.strings.LeaveComment
                    }
                    placeholderTextColor={this.context.fontColor}
                    style={styles.formInput}
                  />

                  <Icon
                    name="send"
                    size={20}
                    color={this.context.fontColor}
                    onPress={() => this.sendMessage()}
                    style={{ padding: 8 }}
                  />
                </View>
              )}
            <Toast message={this.state.errorMessage} />
          </Background>
        </TouchableWithoutFeedback>
      );
    } else {
      return <Background empty />;
    }
  }
}

var styles = StyleSheet.create({
  progress: {
    flex: 1,
    flexDirection: "row",
    overflow: "hidden",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 4,
    marginRight: 4,
    height: 12,
    borderRadius: 6,
  },
  innerProgressCompleted: {
    height: 12,
    backgroundColor: Colors.Text1,
    opacity: 0.65,
  },
  innerProgressRemaining: {
    height: 12,
    backgroundColor: Colors.Text1,
    opacity: 0.3,
  },
  listStyle: {
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5,
  },
  liveDetailsContainerStyle: {
    marginLeft: 10,
    backgroundColor: Colors.Text1,
  },
  formInput: {
    flex: 1,
    fontSize: 16,
    color: Colors.Text1,
    paddingLeft: 10,
    paddingRight: 10,
  },
  descriptionIconStyle: {
    alignItems: "center",
    justifyContent: "center",
  },
  superView: {
    marginLeft: 5,
    marginTop: 3,
    marginRight: 5,
  },
  videoBarStyle: {
    position: "absolute",

    borderColor: "black",
    borderWidth: 0.7,
    top: 0,
    right: 0,
    backgroundColor: "white",
    width: 200,
    height: 60,
    zIndex: 9999,
  },
  imgStyle: { width: 40, height: 40 },

  likeNotSelected: {
    color: "red",
  },
});

export { Player };
