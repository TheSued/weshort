import React from "react";
import {
  View,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
  BackHandler,
  Platform,
  ImageBackground,
} from "react-native";

import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/MaterialIcons";
import { PERMISSIONS, RESULTS, check, request } from "react-native-permissions";
import ImagePicker from "react-native-image-picker";

import { Background } from "../components/common";
import { Header, Modal } from "../components";
import {
  AppText,
  Input,
  Rotate,
  Expand,
  Touchable,
} from "../components/common";

import * as Network from "../network";
import { ActiveCompanyContext } from "../providers/CompanyProvider";
import Routes from "../environments/Routes";
import Colors from "../environments/Colors";

const currentScreen = "Addlive";

class AddLive extends React.Component {
  static contextType = ActiveCompanyContext;
  state = {
    imageSource: "",
    imageSize: {
      height: 180,
      width: 320,
    },
    title: "",
    description: "",
    collections: [],
    showCollections: false,
    loadingCollections: true,
    showError: false,
    message: "",
    cameraPermission: false,
    microphonePermission: false,
    libraryPermission: false,
    liveCreated: false,
    liveData: undefined,
    pageHasFocus: false,
  };

  async loadCollections() {
    data = await Network.getUserCollection();
    if (data) {
      if (data != this.state.collections) {
        this.setState({ collections: data, loadingCollections: false });
      }
    } else {
      this.setState({ loadingCollections: false });
    }
  }

  viewWillFocus() {
    this.setState({ pageHasFocus: true });
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () =>
      this.goBack()
    );
    this.loadCollections();
  }

  viewWillBlur() {
    this.setState({ pageHasFocus: false });

    if (this.state.liveCreated) {
      // resetting states to allow user to use it later, keep edits otherwise
      this.setState({
        imageSource: "",
        title: "",
        description: "",
        collections: [],
        liveCreated: false,
        liveData: undefined,
        errorMessage: "",
      });
    } else {
      this.setState({ errorMessage: "" });
    }
    if (this.backHandler) {
      this.backHandler.remove();
    }
  }

  componentWillUnmount() {
    if (this.backHandler) {
      this.backHandler.remove();
    }
    this.focusUnsubscribe();
    this.blurUnsubscribe();
  }

  async selectImage() {
    let library;
    if (!this.state.libraryPermission) {
      library = await this.askLibraryPermission();
    }
    if (this.state.libraryPermission || library) {
      var options = {
        storageOptions: {
          skipBackup: true,
          path: "images",
        },
      };
      ImagePicker.launchImageLibrary(options, (response) => {
        if (!response.didCancel && !response.error) {
          this.setState({ imageSource: response.uri, image: response });
        }
      });
    } else {
      this.onError(global.strings.NoPermissions);
    }
  }

  async askCameraPermission() {
    let response = 0;
    if (Platform.OS == "android") {
      response = await request(PERMISSIONS.ANDROID.CAMERA);
    } else {
      response = await request(PERMISSIONS.IOS.CAMERA);
    }
    if (response == RESULTS.GRANTED) {
      this.setState({ cameraPermission: true });
      return true;
    } else {
      this.setState({ cameraPermission: false });
      return false;
    }
  }

  async askMicrophonePermission() {
    let response = 0;
    if (Platform.OS == "android") {
      response = await request(PERMISSIONS.ANDROID.RECORD_AUDIO);
    } else {
      response = await request(PERMISSIONS.IOS.MICROPHONE);
    }
    if (response == RESULTS.GRANTED) {
      this.setState({ microphonePermission: true });
      return true;
    } else {
      this.setState({ microphonePermission: false });
      return false;
    }
  }

  async askLibraryPermission() {
    let response = 0;
    if (Platform.OS == "android") {
      response = await request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
    } else {
      response = await request(PERMISSIONS.IOS.PHOTO_LIBRARY);
    }
    if (response == RESULTS.GRANTED) {
      this.setState({ libraryPermission: true });
      return true;
    } else {
      this.setState({ libraryPermission: false });
      return false;
    }
  }

  async checkPermissions() {
    let response = 0;
    if (Platform.OS == "android") {
      response = await Promise.all([
        check(PERMISSIONS.ANDROID.CAMERA),
        check(PERMISSIONS.ANDROID.RECORD_AUDIO),
        check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE),
      ]);
    } else {
      response = await Promise.all([
        check(PERMISSIONS.IOS.CAMERA),
        check(PERMISSIONS.IOS.MICROPHONE),
        check(PERMISSIONS.IOS.PHOTO_LIBRARY),
      ]);
    }
    this.setState({
      cameraPermission: response[0] === RESULTS.GRANTED ? true : false,
      microphonePermission: response[1] === RESULTS.GRANTED ? true : false,
      libraryPermission: response[2] === RESULTS.GRANTED ? true : false,
    });
  }

  componentDidMount() {
    this.focusUnsubscribe = this.props.navigation.addListener("focus", () =>
      this.viewWillFocus()
    );
    this.blurUnsubscribe = this.props.navigation.addListener("blur", () =>
      this.viewWillBlur()
    );
    this.checkPermissions();
  }

  onError(message) {
    this.setState({ message: message }, () =>
      setTimeout(() => this.setState({ showError: true }), 500)
    );
  }

  async goLive() {
    const { liveData, microphonePermission, cameraPermission } = this.state;
    let camera;
    let microphone;
    if (!cameraPermission) {
      camera = await this.askCameraPermission();
    }
    if (!microphonePermission) {
      microphone = await this.askMicrophonePermission();
    }
    if ((microphonePermission || microphone) && (cameraPermission || camera)) {
      this.setState({ liveCreated: false });
      this.props.navigation.navigate("StreamView", {
        previousScreen: "ManageLive",
        streamId: liveData.id,
        streamData: {
          serverLive: liveData.server_live,
          streamKey: liveData.streamKey,
          streamCode: liveData.code,
        },
      });
    } else {
      this.onError(global.strings.StreamPermissions);
    }
  }

  createLive() {
    if (
      this.state.title != "" &&
      this.state.collections.length > 0 &&
      this.state.description != ""
    ) {
      this.setState({ message: "", showError: false });
      setTimeout(async () => {
        if (!this.state.imageSource) {
          liveData = await Network.createLive(
            this.state.title,
            this.state.description,
            this.state.collections[0].id
          );
        } else {
          liveData = await Network.createLiveWithCover(
            this.state.title,
            this.state.description,
            this.state.collections[0].id,
            this.state.image
          );
        }
        if (liveData) {
          this.setState({ liveCreated: true, liveData: liveData });
        } else {
          this.onError(global.strings.NetworkError);
        }
      }, 500);
    } else {
      this.onError(global.strings.MissingFieldLive);
    }
  }

  renderCollectionIcon() {
    if (this.state.collections.length > 1) {
      return (
        <Rotate
          params={{ start: "180deg", end: "0deg" }}
          controller={this.state.showCollections}
          style={styles.collectionIconStyle}
        >
          <Icon name="arrow-drop-up" size={23} color={this.context.fontColor} />
        </Rotate>
      );
    }
  }

  renderCollectionItem(item) {
    return (
      <AppText
        style={[
          styles.listItemStyle,
          item == this.state.collections[0]
            ? styles.listItemActive
            : styles.listItemInactive,
        ]}
      >
        {item.title}
      </AppText>
    );
  }

  selectedCollectionText() {
    if (this.state.loadingCollections) {
      return (
        <AppText
          style={[styles.selectedCollectionStyle, styles.listItemInactive]}
        >
          {" "}
          {global.strings.LoadingCollections}{" "}
        </AppText>
      );
    } else if (
      this.state.collections.length > 0 &&
      !this.state.showCollections
    ) {
      return (
        <AppText style={styles.selectedCollectionStyle}>
          {" "}
          {this.state.collections[0].title}{" "}
        </AppText>
      );
    } else if (!this.state.showCollections) {
      return (
        <AppText
          style={[styles.selectedCollectionStyle, styles.listItemInactive]}
        >
          {" "}
          {global.strings.NoCollection}{" "}
        </AppText>
      );
    }
  }

  selectListItem(item, index) {
    let tmp = this.state.collections;
    if (index != 0) {
      for (i = index - 1; i >= 0; i--) {
        tmp[i + 1] = tmp[i];
      }
      tmp[0] = item;
      this.setState({ collections: tmp });
    }
    this.setState({ showCollections: false });
  }

  measureList() {
    if (this.state.collections.length <= 6) {
      return 50 + 21 * this.state.collections.length;
    } else {
      return 50 + 21 * 6;
    }
  }

  renderCollection() {
    if (this.state.showCollections) {
      return (
        <FlatList
          data={this.state.collections}
          renderItem={({ item, index }) => (
            <View style={styles.flex}>
              <Touchable onPress={() => this.selectListItem(item, index)}>
                {this.renderCollectionItem(item)}
              </Touchable>
            </View>
          )}
          keyExtractor={(item) => String(item.id)}
          style={{ height: this.measureList() }}
        />
      );
    }
  }

  renderDescription() {
    return (
      <View>
        <Input
          style={styles.descriptionStyle}
          placeholder={global.strings.DescriptionPlaceholder}
          autoCorrect={false}
          value={this.state.description}
          onChangeText={(text) => this.setState({ description: text })}
          multiline
          autogrow
          maxHeight={130}
          blurOnSubmit
        />
      </View>
    );
  }

  async goBack() {
    const { previousScreen } = this.props.route.params;
    this.props.navigation.goBack();
  }

  render() {
    if (this.state.pageHasFocus) {
      return (
        <Background
          simple
          header={
            <Header
              onMenuPress={() => this.goBack()}
              onLogoPress={() => this.props.navigation.popToTop()}
            />
          }
        >
          {/* live preview picker */}
          <View style={styles.livePreviewContainerStyle}>
            <Touchable onPress={() => this.selectImage()}>
              <ImageBackground
                source={{
                  uri:
                    this.state.imageSource || this.context.activeCompany.image,
                }}
                style={{
                  height: this.state.imageSize.height,
                  width: this.state.imageSize.width,
                  borderRadius: 16,
                  justifyContent: "flex-end",
                  alignItems: "center",
                  paddingBottom: 16,
                }}
              >
                <AppText>{global.strings.LoadPreview}</AppText>
              </ImageBackground>
            </Touchable>
          </View>

          {/* Error message */}
          <Expand
            params={{ start: 0, end: 60 }}
            controller={this.state.message == "" ? false : true}
          >
            <AppText style={styles.errorTextStyle}>
              {" "}
              {this.state.showError ? this.state.message : ""}{" "}
            </AppText>
          </Expand>

          {/* Title */}
          <View style={styles.titleContainerStyle}>
            <Input
              onChangeText={(text) => this.setState({ title: text })}
              value={this.state.title}
              placeholder={global.strings.TitlePlaceholder}
              style={styles.titleStyle}
            />
          </View>

          {/* Collection */}
          <Expand
            params={{ start: 70, end: this.measureList() }}
            controller={this.state.showCollections}
            style={styles.listContainerStyle}
          >
            <Touchable
              onPress={() => {
                if (this.state.collections.length > 1) {
                  this.setState({
                    showCollections: !this.state.showCollections,
                  });
                }
              }}
            >
              <View style={styles.row}>
                <AppText style={styles.collectionLabelStyle}>
                  {" "}
                  {global.strings.Collection}{" "}
                </AppText>
                {this.renderCollectionIcon()}
              </View>
            </Touchable>
            {this.selectedCollectionText()}
            {this.renderCollection()}
          </Expand>

          {/* Description */}
          <View style={styles.descriptionContainerStyle}>
            {this.renderDescription()}
          </View>

          {/* Footer */}
          <View style={[styles.flex, styles.footerContainerStyle]}>
            <View style={[styles.flex, styles.rowFlex, styles.footerStyle]}>
              <View
                style={[styles.flex, styles.buttonGradientContainerStyle]}
              />
              <LinearGradient
                colors={[Colors.Secondary1, Colors.Secondary2]}
                start={{ x: 0.0, y: 0.5 }}
                end={{ x: 1.0, y: 0.5 }}
                style={styles.buttonGradientStyle}
              >
                <Touchable
                  onPress={() => this.createLive()}
                  style={[styles.flex, styles.buttonStyle]}
                >
                  <View style={[styles.flex, styles.buttonIconContainerStyle]}>
                    <Icon
                      name="videocam"
                      size={50}
                      style={styles.buttonIconStyle}
                    />
                  </View>
                </Touchable>
              </LinearGradient>
            </View>
          </View>

          <Modal
            visible={this.state.liveCreated}
            style={{
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.ModalBackground,
            }}
          >
            <View
              style={{
                borderRadius: 5,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: Colors.ModalColor,
                elevation: 1,
              }}
            >
              <AppText
                style={{
                  fontSize: 16,
                  marginBottom: 8,
                  paddingTop: 16,
                  paddingHorizontal: 16,
                  color: this.context.fontColor,
                }}
              >
                {global.strings.LiveCreated}
              </AppText>
              <View
                style={{
                  alignSelf: "stretch",
                  borderBottomLeftRadius: 5,
                  borderBottomRightRadius: 5,
                }}
              >
                <Touchable
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    elevation: 2,
                    backgroundColor: this.context.backgroundColor,
                    paddingHorizontal: 0,
                  }}
                  onPress={() => this.goLive()}
                >
                  <AppText
                    style={{
                      fontSize: 16,
                      fontWeight: "500",
                      color: this.context.fontColor,
                      paddingVertical: 16,
                    }}
                  >
                    {global.strings.GoLive}
                  </AppText>
                </Touchable>
                <Touchable
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    elevation: 2,
                    paddingHorizontal: 0,
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                  }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <AppText
                    style={{
                      fontSize: 16,
                      paddingBottom: 16,
                      color: Colors.PlaceholderText,
                    }}
                  >
                    {global.strings.Later}
                  </AppText>
                </Touchable>
              </View>
            </View>
          </Modal>
        </Background>
      );
    } else {
      return <Background empty />;
    }
  }
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: "row",
  },

  // live preview
  livePreviewContainerStyle: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: "black",
    borderRadius: 3,
  },
  livePreviewStyle: {
    height: 180,
    width: 320,
    borderRadius: 16,
  },

  // error text
  errorTextStyle: {
    alignSelf: "center",
    color: Colors.TextError,
    fontSize: 17,
    marginTop: 15,
    elevation: 1,
  },

  // list
  collectionIconStyle: {
    marginTop: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  listContainerStyle: {
    backgroundColor: Colors.Background1,
    marginTop: 5,
    marginRight: 20,
    marginLeft: 20,
  },
  collectionLabelStyle: {
    color: Colors.PlaceholderText,
    fontSize: 20,
    lineHeight: 23,
    elevation: 1,
    marginLeft: 10,
    marginTop: 15,
  },
  selectedCollectionStyle: {
    color: Colors.Text1,
    fontSize: 18,
    marginLeft: 13,
    elevation: 1,
  },
  listItemStyle: {
    fontSize: 18,
    marginLeft: 17,
    elevation: 1,
  },
  listItemActive: {
    color: Colors.Text1,
  },
  listItemInactive: {
    color: Colors.PlaceholderText,
  },

  // live data
  titleContainerStyle: {
    backgroundColor: Colors.Background1,
    paddingBottom: 5,
    paddingTop: 5,
    marginTop: 15,
    marginLeft: 20,
    marginRight: 20,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  titleStyle: {
    fontSize: 20,
    color: Colors.Text1,
    paddingLeft: 15,
  },
  descriptionContainerStyle: {
    paddingBottom: 5,
    paddingTop: 5,
    backgroundColor: Colors.Background1,
    marginTop: 5,
    marginRight: 20,
    marginLeft: 20,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  descriptionStyle: {
    color: Colors.Text1,
    fontSize: 20,
    paddingLeft: 15,
    textAlignVertical: "top",
  },

  // footer
  footerContainerStyle: {
    alignItems: "flex-end",
  },
  footerStyle: {
    justifyContent: "space-around",
    alignItems: "flex-end",
  },
  buttonGradientContainerStyle: {
    alignItems: "center",
    justifyContent: "center",
  },
  buttonGradientStyle: {
    height: 80,
    width: 80,
    borderRadius: 50,
    elevation: 1,
    margin: 8,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonStyle: {
    alignItems: "center",
    justifyContent: "center",
  },
  buttonIconContainerStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonIconStyle: {
    color: Colors.Text1,
    fontWeight: "600",
    alignSelf: "center",
  },
  backButtonStyle: {
    justifyContent: "center",
    alignItems: "center",
    margin: 8,
  },
});

export { AddLive };
