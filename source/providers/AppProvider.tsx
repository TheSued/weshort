import React from "react";
import { AuthProvider, ActiveCompanyProvider, UtilsProvider } from "./";

export function AppProvider(props: any) {
  return (
    <UtilsProvider>
      <AuthProvider>
        <ActiveCompanyProvider>{props.children}</ActiveCompanyProvider>
      </AuthProvider>
    </UtilsProvider>
  );
}
