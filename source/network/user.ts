import { Platform } from "react-native";

import Routes from "../environments/Routes";
import http from "./http";
import Storage from "../services/Storage";
import { AppCompanyId } from "../environments/AppEnvironment";

export async function login(email: string, password: string) {
  const formdata = new FormData();
  formdata.append("email", email);
  formdata.append("password", password);
  formdata.append("app", 1);
  formdata.append("device_platform", Platform.OS);
  const loginResponse = await http.login(Routes.Login, formdata);
  if (loginResponse !== -1 && loginResponse !== -2) {
    Storage.saveToken(loginResponse);
  }
  return loginResponse;
}

export function setDeviceToken(deviceToken: string) {
  const formdata = new FormData();
  formdata.append("device_token", deviceToken);
  formdata.append("device_platform", Platform.OS);

  http.post(Routes.SetDeviceToken, formdata);
}

export async function getUserData(companyId: number | false) {
  let url = `${Routes.ProfileDataRoute}&company=${companyId}`;
  const data = await http.get(url);
  if (data.length && data[0].status !== "success") {
    Storage.deleteToken();
    return null;
  }
  return data;
}

export async function getUserCollection() {
  const collections = await http.get(Routes.GetCollections);
  return collections;
}

export async function changeCompany(idCompany: number) {
  const url = `${Routes.changeCompany}&idc=${idCompany}`;
  const data = await http.get(url);
  if (data && data[0] && data[0].status === "success") {
    // TODO: set new companyId = idc
  }
}

export async function getScheduledLives(step: number) {
  const url = `${Routes.GetVideo}&scheduled=1&mine=1&step=${step}`;
  const scheduled = (await http.get(url)) || [];
  return scheduled;
}
export async function ChangeUserLanguage(language: string) {
  const formdata = new FormData();
  formdata.append("language", language);
  const newLanguage = await http.post(Routes.ChangeLanguage, formdata);
  return newLanguage;
}

export async function createLive(
  title: string,
  description: string,
  id_collection: string | number,
  file: any
) {
  const formdata = new FormData();
  formdata.append("title", title);
  formdata.append("description", description);
  formdata.append("collection", id_collection);

  if (file) {
    formdata.append("file1", {
      uri: file,
      type: "video/mp4",
      name: "video.mp4",
    });
  }
  const liveData = (await http.post(Routes.AddLive, formdata)) || [];
  if (liveData[0] && liveData[0].status === "success") {
    return liveData[0];
  }
  return null;
}

export async function createLiveWithCover(
  title: string,
  description: string,
  collection_id: number,
  image: any
) {
  const response =
    (await http.post(Routes.AddLiveWithCover, new FormData())) || [];
  if (response[0] && response[0].url) {
    const uploaded = await http.uploadImage(
      response[0].url,
      title,
      description,
      collection_id,
      image
    );
    return uploaded;
  }
  return null;
}

export async function registerUser(
  email: string,
  password: string,
  repeatPassword: string,
  privacy: boolean,
  promotions: boolean
) {
  const formdata = new FormData();
  formdata.append("id_company", AppCompanyId);
  formdata.append("email", email);
  formdata.append("password", password);
  formdata.append("passwordConfirm", repeatPassword);
  formdata.append("checkBoxTermsAndCondition", 1);
  formdata.append("checkPrivacy", privacy ? 1 : 0);
  formdata.append("checkPromotions", promotions ? 1 : 0);
  const registerResult =
    (await http.postNoAuth(Routes.Register, formdata)) || [];
  console.log(
    registerResult,
    "<=================== this is the registration result"
  );
  return registerResult[0];
}
