import { Alert, Platform } from "react-native";
import Storage from "../services/Storage";

let token: string = "";

export async function getToken() {
  if (token) {
    return token;
  }
  token = await Storage.getToken();
  return token;
}

export function deleteToken() {
  token = "";
}
const http = {
  login: async (url: string, formdata: FormData): Promise<any> => {
    deleteToken();
    const headers = new Headers({
      Accept: "application/json",
      "Content-Type": "multipart/form-data",
    });
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: headers,
        body: formdata,
      });
      const responseData = await response.text();
      const data = JSON.parse(responseData);
      console.warn("formData: ", data);

      if (data[0].status === "success") {
        return data[0].token;
      } else {
        return -1;
      }
    } catch (error) {
      return -2;
    }
  },

  get: async (url: string): Promise<any> => {
    try {
      const auth = await getToken();
      if (!auth) {
        throw new Error("missing user data");
      }
      const headers = new Headers({
        Authorization: token,
        Accept: "application/json",
      });
      const response = await fetch(url, {
        method: "GET",
        headers: headers,
      });
      const data = await response.json();
      return data;
    } catch (error) {
      logError(url, error);
      return error;
    }
  },

  getNoAuth: async (url: string): Promise<any> => {
    try {
      const headers = new Headers({
        Accept: "application/json",
      });
      const response = await fetch(url, {
        method: "GET",
        headers: headers,
      });
      const data = await response.json();
      return data;
    } catch (error) {
      logError(url, error);
      return null;
    }
  },
  //nel caso di errore non metti campi custom, procedi con il pagamento con stripe
  getNoAuthCart: async (url: string): Promise<any> => {
    try {
      const headers = new Headers({
        Accept: "application/json",
      });
      const response = await fetch(
        "https://testarea.teyuto.tv/app/api/v1/business/?f=get_custom_field",
        {
          method: "GET",
          headers: headers,
        }
      );
      const data = await response.json();
      return data;
    } catch (error) {
      logError(url, error);
      return null;
    }
  },

  post: async (url: string, formdata: FormData): Promise<any> => {
    try {
      const auth = await getToken();
      if (!auth) {
        throw new Error("missing user data");
      }
      const headers = new Headers({
        Authorization: auth,
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
      });

      try {
        const response = await fetch(url, {
          method: "POST",
          headers: headers,
          body: formdata,
        });

        const data = await response.json();

        return data || true;
      } catch (error) {
        return error;
      }
    } catch (error: any) {
      logError(url, error);
      return { url, error };
    }
  },

  postNoAuth: async (url: string, formdata: FormData): Promise<any> => {
    try {
      const headers = new Headers({
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
      });
      const response = await fetch(url, {
        method: "POST",
        headers: headers,
        body: formdata,
      });
      const data = await response.json();
      return data || true;
    } catch (error) {
      logError(url, error);
      return null;
    }
  },

  uploadImage: (
    url: string,
    title: string,
    description: string,
    collection_id: number,
    image: any
  ) => {
    try {
      let xhr = new XMLHttpRequest();
      xhr.open("POST", url);
      let formdata = new FormData();
      formdata.append("title", title);
      formdata.append("description", description);
      formdata.append("collection", collection_id);

      formdata.append("file", {
        name: image.fileName,
        type: image.type,
        uri:
          Platform.OS === "android"
            ? image.uri
            : image.uri.replace("file://", ""),
      });
      xhr.setRequestHeader("Authorization", token);

      xhr.send(formdata);
      return true;
    } catch (error) {
      logError(url, error);
      return null;
    }
  },
};

const logError = (url: string, error: Error): void => {
  console.log("httpError: ", url, error);
};

export default http;
