import React from 'react';
import { StyleSheet, View, FlatList, StatusBar } from 'react-native';

import * as Network from '../network';

import { ActiveCompanyContext } from '../providers/CompanyProvider';
import { AppText, Touchable } from './common';
import Colors from '../environments/Colors';

class SideMenu extends React.Component {
  static contextType = ActiveCompanyContext;

  state = {
    companies: [],
    activeCompany: {},
  };

  componentDidMount() {
    this.setState({
      activeCompany: this.context.activeCompany,
      companies: this.context.companies,
    });
  }

  changeCompany(index) {
    const currentCompany = this.state.activeCompany;
    const nextCompany = this.state.companies[index];
    if (currentCompany.id !== nextCompany.id) {
      Network.changeCompany(nextCompany.id);
      this.setState({ activeCompany: nextCompany }, () => {
        this.context.changeCompany(nextCompany);
        this.props.changeCompany();
      });
    } else {
      this.props.changeCompany();
    }
  }

  render() {
    const { activeCompany } = this.context;
    return (
      <View
        style={{
          flex: 1,
          alignSelf: 'stretch',
          margin: 8,
          marginTop: StatusBar.currentHeight + 4,
          backgroundColor:
            activeCompany.background_color !== null && activeCompany.background_color !== ''
              ? activeCompany.background_color
              : Colors.Background2,
        }}>
        <FlatList
          data={this.state.companies}
          keyExtractor={(item) => String(item.id)}
          style={{ flex: 1 }}
          renderItem={({ item, index }) => (
            <Touchable
              style={[
                { padding: 8, margin: 8 },
                item.id === activeCompany.id ? { backgroundColor: 'rgba(255,255,255,.2)' } : null,
              ]}
              onPress={() => this.changeCompany(index)}>

              <AppText
                style={{
                  fontSize: 20,
                  paddingTop: 12,
                  paddingBottom: 12,
                }}>
                {item.name_company}
              </AppText>
            </Touchable>
          )}
        />
      </View>
    );
  }
}

export { SideMenu };
