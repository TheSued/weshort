import React from 'react';
import { View, StyleSheet, ImageBackground, FlatList } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import { AppText, Touchable, } from './common';

import Routes from '../environments/Routes';
import Colors from '../environments/Colors';

const CatalogList = (props) => {
  const { data, label, onItemPress, onEndReached, defaultPreview } = props;

  if (data && data.length) {
    return (
      <React.Fragment>
        <View>
          {/*<AppText style={styles.catalogLabelStyle}>{label}</AppText>*/}
          <FlatList
            showsHorizontalScrollIndicator={false}
            //horizontal
            data={data}
            renderItem={({ item }) =>
              <View style={styles.searchCatolgStyle}>
                <ImageBackground
                  resizeMode={'contain'}
                  source={{ uri: item.img_preview || defaultPreview }}
                  style={styles.livePreviewStyle}
                  imageStyle={{ borderRadius: 1, }}
                  style={{
                    width: global.screenProp.width,
                    height: (global.screenProp.width / 16) * 9,
                    padding: 15,
                  }}>
                  <Touchable onPress={() => onItemPress(item)}>
                    {/*<LinearGradient
                      colors={['black', 'transparent']}
                      start={{ x: 0.2, y: 1.0 }}
                      end={{ x: 0.2, y: 0.0 }}
                    style={styles.translucentStyle}> </LinearGradient> */}

                    {item.duration ? (
                      <AppText style={styles.durationStyle}>
                        {item.duration}
                      </AppText>
                    ) : <AppText></AppText>}


                  </Touchable>
                </ImageBackground>
                <AppText style={styles.liveTitleStyle}>{item.title}</AppText>
              </View>
            }
            keyExtractor={(item) => String(item.id)}
            style={styles.listStyle}
            onEndReached={onEndReached}
          />

        </View>
      </React.Fragment>);
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  listStyle: {
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 28,
  },
  livePreviewStyle: {
    width: 240,
    elevation: 1,
    alignSelf: 'center',
  },
  liveDetailsContainerStyle: {
    marginLeft: 10,
  },
  liveTitleStyle: {
    fontWeight: 'bold',
    fontSize: 15,
    opacity: 0.7,
    backgroundColor: 'black',
  },
  translucentStyle: {
    height: 135,
    width: 240,
    justifyContent: 'space-between',
    borderRadius: 1,
    padding: 4,
    paddingBottom: 8,
  },
  catalogLabelStyle: {
    fontSize: 20,
    fontWeight: '700',
    lineHeight: 23,
    elevation: 1,
    marginLeft: 10,
    marginVertical: 4,
  },
  durationStyle: {
    fontSize: 13,
    fontWeight: '700',
    padding: 4,
    marginRight: 10,
    backgroundColor: 'rgb(48,48,48)',
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',

  },

  boxSearchCatolgStyle: {

    justifyContent: 'center',
  },

  searchCatolgStyle: {
    marginBottom: 30

  },
});

export { CatalogList };
