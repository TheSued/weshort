import { StyleSheet } from "react-native";
import Colors from "../../environments/Colors";

export const homepageStyles = StyleSheet.create({
  listStyle: {
    // paddingLeft: 5,
    // paddingRight: 5,
    marginBottom: 40,
  },
  livePreviewStyle: {
    height: 135,
    width: 240,
    backgroundColor: "black",
    borderRadius: 4,
    marginHorizontal: 2,
  },
  liveTitleStyle: {
    fontWeight: "bold",
    fontSize: 15,
    maxWidth: 230,
    marginTop: 7,
    marginLeft: 10,
    opacity: 0.7,
  },

  imageRadius: { borderRadius: 4 },

  translucentStyle: {
    height: 135,
    width: 240,
    justifyContent: "space-between",
    borderRadius: 1,
    padding: 4,
    paddingBottom: 8,
  },

  listTitleStyle: {
    fontSize: 18,
    fontWeight: "700",
    elevation: 1,
    marginLeft: 5,
    marginRight: 7,
  },

  sfogliaStyle: {
    fontSize: 13,
    elevation: 1,
    fontWeight: "700",
    color: "rgb(129, 129, 130)",
    alignItems: "center",
  },

  titlePress: {
    justifyContent: "flex-start",
    flexDirection: "row",
    marginBottom: 10,
    height: 30,
    alignItems: "center",
  },

  durationStyle: {
    fontSize: 13,
    fontWeight: "700",
    padding: 4,
    backgroundColor: "rgb(48,48,48)",
    color: "white",
    alignSelf: "flex-end",
    marginTop: 3,
    marginRight: 3,
    marginLeft: "auto",
  },

  iconStyle: {
    color: "rgb(129, 129, 130)",
    textAlign: "auto",
  },

  lockStyle: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
