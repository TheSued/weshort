import React from "react";
import { View, ImageBackground, SafeAreaView, Platform } from "react-native";

import NetInfo from "@react-native-community/netinfo";

import { ActiveCompanyContext } from "../../../providers/CompanyProvider";
import Colors from "../../../environments/Colors";
import styles from "./styles";
import BackgroundUI from "./BackgroundUI";
import ScrollViewUIandroid from "./ScrollViewUI.android";
import ScrollViewUIios from "./ScrollViewUI.ios";

interface BackgroundProps {
  header?: any;
  style?: any;
  fullscreen?: boolean;
  type?: "login" | "simple" | "live";
  login?: boolean;
  simple?: boolean;
  noscroll?: boolean;
  empty?: boolean;
}

interface BackgroundState {
  isConnected: boolean;
  backgroundColor: string;
  fontColor: string;
}

class Background extends React.PureComponent<BackgroundProps, BackgroundState> {
  static contextType = ActiveCompanyContext;

  constructor(props: BackgroundProps) {
    super(props);
    this.state = {
      isConnected: false,
      backgroundColor: Colors.Background2,
    };
  }

  componentDidMount() {
    NetInfo.fetch().then((state) => {
      this.setState({
        isConnected: state.isConnected,
        backgroundColor: this.getBackgroundColor(),
      });
    });
  }

  getBackgroundColor() {
    const { activeCompany } = this.context;
    if (this.props.fullscreen) {
      // player e streaming in fullscreen
      return "black";
    } else if (activeCompany && activeCompany.background_color === "#fff") {
      activeCompany.background_color = Colors.Background3;
      activeCompany.font_color = Colors.Text3;
    } else {
      return this.context.backgroundColor;
    }
  }

  getFontColor() {
    const { activeCompany } = this.context;
    if (activeCompany && activeCompany.font_color)
      return activeCompany.font_color;
  }

  render() {
    const { backgroundColor } = this.state;
    if (this.props.empty) {
      return (
        <View
          style={{ flex: 1, backgroundColor: this.context.backgroundColor }}
        />
      );
    } else if (this.props.login && this.state.isConnected) {
      return (
        // login background
        <ImageBackground
          source={{
            uri: this.context.activeCompany.image,
          }}
          style={[
            styles.flex,
            this.props.style,
            { backgroundColor: this.context.backgroundColor },
          ]}
        >
          {Platform.OS === "android" ? (
            <ScrollViewUIandroid
              style={[this.props.style]}
              header={this.props.header}
              fullscreen={this.props.fullscreen}
            >
              {this.props.children}
            </ScrollViewUIandroid>
          ) : (
            <ScrollViewUIios
              style={[this.props.style]}
              header={this.props.header}
              fullscreen={this.props.fullscreen}
            >
              {this.props.children}
            </ScrollViewUIios>
          )}
        </ImageBackground>
      );
    } else if (this.props.noscroll) {
      // used by StreamView
      return (
        <SafeAreaView
          style={[
            styles.flex,
            this.props.style,
            { backgroundColor: this.context.backgroundColor },
          ]}
        >
          {this.props.children}
        </SafeAreaView>
      );
    } else if (this.props.simple) {
      return (
        <BackgroundUI
          backgroundColor={this.context.backgroundColor}
          style={this.props.style}
          header={this.props.header}
          fullscreen={this.props.fullscreen}
        >
          {this.props.children}
        </BackgroundUI>
      );
    } else {
      return Platform.OS === "android" ? (
        <ScrollViewUIandroid
          fullscreen={this.props.fullscreen}
          backgroundColor={this.context.backgroundColor}
          style={this.props.style}
          header={this.props.header}
        >
          {this.props.children}
        </ScrollViewUIandroid>
      ) : (
        <ScrollViewUIios
          fullscreen={this.props.fullscreen}
          backgroundColor={this.context.backgroundColor}
          style={this.props.style}
          header={this.props.header}
        >
          {this.props.children}
        </ScrollViewUIios>
      );
    }
  }
}

export { Background };
