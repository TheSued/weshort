"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_native_1 = require("react-native");
var styles_1 = require("./styles");
var AppStatusBar_1 = require("../AppStatusBar/AppStatusBar");
var BackgroundUI = function (props) {
    var style = props.style, header = props.header, children = props.children, backgroundColor = props.backgroundColor, fullscreen = props.fullscreen;
    var flex = styles_1["default"].flex, paddingBottom = styles_1["default"].paddingBottom, paddingTop = styles_1["default"].paddingTop;
    return (react_1["default"].createElement(react_1["default"].Fragment, null,
        react_1["default"].createElement(AppStatusBar_1["default"], { fullscreen: props.fullscreen }),
        react_1["default"].createElement(react_native_1.SafeAreaView, { style: [
                flex,
                fullscreen
                    ? {
                        justifyContent: "center",
                        alignItems: "center"
                    }
                    : null,
                {
                    backgroundColor: props.fullscreen ? "black" : backgroundColor
                },
                fullscreen ? null : paddingTop,
                style,
            ] },
            header || null,
            react_1["default"].createElement(react_native_1.View, { style: [flex, paddingBottom, style] }, children))));
};
exports["default"] = BackgroundUI;
