import React, { Component } from 'react';
import {
	View,
	Animated,
} from 'react-native';

class Rotate extends Component {

	componentWillMount() {
		this.spinValue = new Animated.Value(0)
	}

	componentDidUpdate() {
		Animated.spring(
		  this.spinValue,
		  {
		    toValue: this.props.controller ? 1 : 0,
			duration: 800,
			useNativeDriver: true,
		    // perspective: 1000
		  }
		).start()
	}


	render() {
		return(
				<Animated.View
					style= {{ 
						transform: [{ rotate: this.spinValue.interpolate({
														  inputRange: [0, 1],
														  outputRange: [this.props.params.start, this.props.params.end]
														}) },
													{perspective: 1000},],
						...this.props.style }}>
					{this.props.children}
				</Animated.View>
			)
	}
}


export { Rotate };