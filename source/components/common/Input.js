import React, { Component } from "react";
import { TextInput, Platform } from "react-native";

import Colors from "../../environments/Colors";
import { Touchable } from "./Touchable";
import { ActiveCompanyContext } from "../../providers";

class Input extends Component {
  static contextType = ActiveCompanyContext;
  textInput;

  render() {
    const { style, underlineColorAndroid, ...rest } = this.props;

    if (Platform.isTV) {
      return (
        <Touchable
          onPress={() => {
            this.textInput && this.textInput.focus();
          }}
        >
          <TextInput
            ref={(ref) => (this.textInput = ref)}
            style={[
              {
                color: this.context.activeCompany.font_color || Colors.Text1,
              },
              style,
            ]}
            placeholderTextColor={Colors.PlaceholderText}
            underlineColorAndroid={
              underlineColorAndroid ? underlineColorAndroid : "transparent"
            }
            selectionColor={Colors.Secondary1}
            {...rest}
          />
        </Touchable>
      );
    } else if (Platform.OS === "android") {
      return (
        <TextInput
          style={[
            {
              color: this.context.activeCompany.font_color || Colors.Text1,
            },
            style,
          ]}
          placeholderTextColor={Colors.PlaceholderText}
          underlineColorAndroid={
            underlineColorAndroid ? underlineColorAndroid : "transparent"
          }
          selectionColor={Colors.Secondary1}
          {...rest}
        />
      );
    } else if (Platform.OS === "ios") {
      return (
        <TextInput
          style={[
            { color: this.context.activeCompany.font_color || Colors.Text1 },
            style,
            underlineColorAndroid
              ? {
                  borderBottomWidth: 1,
                  borderBottomColor: underlineColorAndroid,
                }
              : {},
          ]}
          placeholderTextColor={Colors.PlaceholderText}
          selectionColor={Colors.Secondary1}
          {...rest}
        />
      );
    }
  }
}

export { Input };
