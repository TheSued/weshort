import React, { useContext, useState, useEffect } from "react";
import {
  View,
  Text,
  ImageBackground,
  FlatList,
  PanResponder,
  Animated,
  Pressable,
  TouchableWithoutFeedback,
} from "react-native";
import Carousel, { ParallaxImage } from "react-native-snap-carousel";
import LinearGradient from "react-native-linear-gradient";

import { AppText, Touchable } from "../";
import { slideshowStyles } from "./SlideshowStyles";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faPlay } from "@fortawesome/free-solid-svg-icons";
import { ActiveCompanyContext } from "../../../providers/CompanyProvider";
import { StyleSheet, Dimensions, Platform } from "react-native";
import { colors } from "./index.style";

const Slideshow = (props: any) => {
  const activeCompany: any = useContext(ActiveCompanyContext);
  const { width: viewportWidth, height: viewportHeight } =
    Dimensions.get("window");

  function wp(percentage: any) {
    const value = (percentage * global.screenProp.width) / 100;
    return Math.round(value);
  }
  const slideWidth = wp(88);
  const slideHeight = (slideWidth * 9) / 16;

  const [sliderWidth, setSliderWidth] = useState<number>(viewportWidth);
  let _carousel;

  const _renderItem = ({ item, index }: any, parallaxProps: any) => {
    const { onItemPress, defaultPreview }: any = props;
    return (
      <View
        style={{ width: slideWidth, height: slideHeight, marginBottom: 80 }}
      >
        <Pressable
          style={({ pressed }) => [
            {
              width: slideWidth,
              height: slideHeight,
              borderRadius: 4,
              opacity: pressed ? 0.6 : 1,
            },
          ]}
          onPress={() => onItemPress(item)}
        >
          {({ pressed }) => (
            <ParallaxImage
              source={{ uri: item.img_preview || defaultPreview }}
              containerStyle={{
                flex: 1,
                marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
                backgroundColor: activeCompany.backgroundColor,
                borderRadius: 4,
              }}
              fadeDuration={1000}
              spinnerColor={activeCompany.buttonColor}
              style={[
                {
                  opacity: pressed ? 0.4 : 1,
                },
                styles.image,
              ]}
              parallaxFactor={0.4}
              {...parallaxProps}
            />
          )}
        </Pressable>
        <Pressable
          style={({ pressed }) => [
            {
              borderRadius: 4,
              opacity: pressed ? 0.4 : 1,
            },
          ]}
          onPress={() => onItemPress(item)}
        >
          <View
            style={{
              padding: 10,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                color: activeCompany.fontColor,
                fontSize: 16,
                marginRight: 9,
                fontWeight: "bold",
              }}
            >
              {item.title}
            </Text>
          </View>
        </Pressable>
      </View>
    );
  };

  const { data }: any = props;

  if (data && data.length) {
    return (
      <>
        <Carousel
          ref={(c: any) => {
            _carousel = c;
          }}
          sliderWidth={sliderWidth}
          itemWidth={slideWidth}
          itemHeight={slideHeight + 80}
          data={data}
          renderItem={_renderItem}
          layout={"default"}
          hasParallaxImages={true}
        />
      </>
    );
  } else {
    return null;
  }
};

export { Slideshow };

const styles = StyleSheet.create({
  image: {
    resizeMode: "contain",
    borderRadius: 4,
  },
});
