import React from "react";
import { View, StyleSheet, ImageBackground, FlatList } from "react-native";

import LinearGradient from "react-native-linear-gradient";

import { AppText, Touchable } from "./common";
import { ActiveCompanyContext, UtilsContext } from "../providers";

import Routes from "../environments/Routes";
import Colors from "../environments/Colors";

class BundleList extends React.Component {
  static contextType = ActiveCompanyContext;
  render() {
    const { data, label } = this.props;

    if (data && data.length) {
      return (
        <View style={{ marginVertical: 16 }}>
          <AppText style={styles.catalogLabelStyle}>{label}</AppText>
          {/* list */}
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            data={data || []}
            renderItem={({ item }) => (
              <ImageBackground
                resizeMode={"cover"}
                source={{
                  uri:
                    item.img_preview ||
                    this.context.activeCompany.default_video_image,
                }}
                style={[styles.livePreviewStyle, { marginRight: 2 }]}
                imageStyle={[styles.livePreviewStyle]}
              >
                <Touchable
                  onPress={() => this.props.onItemPress(item)}
                  style={styles.liveButtonStyle}
                >
                  <LinearGradient
                    colors={["black", "transparent"]}
                    start={{ x: 0.2, y: 1.0 }}
                    end={{ x: 0.2, y: 0.0 }}
                    style={styles.translucentStyle}
                  >
                    {item.parent && (
                      <AppText style={{ fontWeight: "500", color: "grey" }}>
                        {global.strings.Bundle}
                      </AppText>
                    )}
                    <View style={{}}>
                      <AppText style={styles.liveTitleStyle}>
                        {item.title}
                      </AppText>
                    </View>
                  </LinearGradient>
                </Touchable>
              </ImageBackground>
            )}
            keyExtractor={(item) =>
              item.id ? String(item.id) : String(item.id_parent)
            }
            style={styles.listStyle}
            onEndReached={this.props.onEndReached}
            ListFooterComponent={<View style={{ width: 50 }} />}
          />
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  // list
  listStyle: {
    padding: 5,
  },
  livePreviewStyle: {
    height: 135,
    width: 240,
    borderRadius: 3,
  },
  translucentStyle: {
    flex: 1,
    justifyContent: "flex-end",
    padding: 4,
    paddingBottom: 8,
    borderRadius: 3,
  },
  liveButtonStyle: {
    flex: 1,
    justifyContent: "flex-end",
    backgroundColor: "transparent",
    borderRadius: 8,
  },
  liveTitleStyle: {
    fontWeight: "bold",
    fontSize: 18,
  },
  liveNVideoStyle: {
    color: Colors.PlaceholderText,
    fontSize: 16,
  },
  catalogLabelStyle: {
    marginTop: 4,
    fontSize: 20,
    fontWeight: "700",
    lineHeight: 23,
    elevation: 1,
    marginLeft: 10,
    marginBottom: 7,
  },
});

export { BundleList };
